/****************************************************************************************************** 	
  	Author		
        -------
	I.TATAJI	10347	tatajiirrinki@gmail.com	    Cell : 9704147867

        Purpose:
        --------
		--Program to implement single linked list by using the member functions adding at given   position,delete at given            			  position,updating the given positioned record.   
		--Here we have taken the data member as name.
******************************************************************************************************/
#include<iostream>
#include<stdio.h>
#include<stdlib.h>

using namespace std;
// creating mylinklist class
class MyLinkList {
	// declaring members for mylinklist class
	int no_of_records;
	
	struct List {
		string name;
		struct List *next;
	}*head;
	public:
	// parametrised constructor for mylinklist class
	MyLinkList(string name) {
		head=new struct List;
		head->name=name;
		head->next=NULL;
		no_of_records=1;
	}
	// method for add data in the list in specific position
	void addAtPosition(string name,int position) {
		struct List *new_record=new struct List;
                new_record->name=name;
		if (position <0 || position > no_of_records+1) {
			cout<< "cannot add the record at position:"<<position<<endl;
		}
		else if(position==0) {
			new_record->next=head;
			head=new_record;
			no_of_records++;
			return;
		}
		else if(position==no_of_records+1) {
			struct List *temp;
			if(head==NULL) {
				head=new_record;
				no_of_records++;
				return;
			}
			for(temp=head;temp->next!=NULL;temp=temp->next);
			temp->next=new_record;
			new_record->next=NULL;
			no_of_records++;
			return;
		}
		else {
			int index;
			struct List *current_record,*previous_record;
			current_record=head;
			for(index=1;index!=position+1;index++) {
				previous_record=current_record;
				current_record=current_record->next;
			}
			previous_record->next=new_record;
			new_record->next=current_record;
			no_of_records++;
			return;
		}
	}

	// method for displaying the data in the list
	void displayRecords() {
		if(head==NULL) {
			cout<<"List is empty."<<endl;
			return;
		}
		struct List *temp;
		cout<<"no.of records:"<<no_of_records<<endl;
		cout<<"\t**********LIST*********"<<endl;
		for(temp = head;temp!=NULL;temp = temp->next) {
			cout<<"\t\t"<<temp->name<<endl;
		}
		delete temp;
		return;
	}
	// method for delete data at beginning 
	void deleteFromBeginning() {

		struct List *temp=head;
		head=head->next;
		delete temp;
		temp=NULL;
		no_of_records--;
		return;
	}
	// method for delete data at end
	void deleteFromEnding() {
		struct List *temp,*previous;
		for(temp=head;temp->next!=NULL;previous=temp,temp=temp->next);
		previous->next=NULL;
		delete temp;
		temp=NULL;
		no_of_records--;
		return;
	}
	// method for delete data in the list at specific position
	void deleteAtPosition(int position) {

		if (position <=0 || position > no_of_records) {
			cout<< "cannot delete the record at position:"<<position<<endl;
		}
		else if(position==1) {

			deleteFromBeginning();
			return;
		}
		else if(position==no_of_records) {
			deleteFromEnding();
			return;
		}
		else {
			int index;
			struct List *current_record,*previous_record;
			current_record=head;
			for(index=1;index!=position;index++) {
				previous_record=current_record;
				current_record=current_record->next;
			}
			previous_record->next=current_record->next;
			delete current_record;
			current_record=NULL;
			no_of_records--;
			return;
		}
	}
	// method for update the data present in the list
	void updateRecord(int position) {
		if(position<=0 || position > no_of_records) {
			cout<<"cannot update the record at that position:"<<position<<endl;
			return;
		}
		int index;
		struct List *temp=head;
		for(index=1;index<position;index++) {
			temp=temp->next;
		}
		cout<<"enter the updated name:"<<endl;
		cin>>temp->name;
		return;
	}
	// destructor for mylinklist class
	~MyLinkList() {
		delete head;
	}
};

int main(void) {
	// creating object for mylinklist class
	MyLinkList mylist("naveena");
	// declaring variables
	string name;
	int position;
	int choice;
	while(1) {
		cout<<"*****MENU*****"<<endl;
		cout<<"1.add record at given position\n2.delete at given position\n3update record\n4.display all records\n5.exit"<<endl;
		cout<<"enter your choice:"<<endl;
		cin>>choice;
		switch(choice) {
			case 1:
					cout<<"enter name to be add:"<<endl;
					cin>>name;
					cout<<"enter the position at which u want to add:"<<endl;
					cin>>position;
				// calling addatposition method to add data into list
				mylist.addAtPosition(name,position);
				break;
			case 2:
					cout<<"enter the position:"<<endl;
					cin>>position;
					// calling deleteatposition method to delete the data from list
					mylist.deleteAtPosition(position);
				break;

			case 3:
				mylist.displayRecords();
				cout<<"enter position at which you want to update the record:"<<endl;	
				cin>>position;
				// calling updaterecord method for updating the data in list
				mylist.updateRecord(position);
			case 4:
				// displaying data present in the list
				mylist.displayRecords();
				break;
			case 5:
				exit(0);
			default: 
				cout<<"invalid choice"<<endl;

		}
	} 


}
/*********************************************************************************************************
OUTPUT:
-------
  *****MENU*****
1.add record at given position
2.delete record at given position
3.update record
4.display all records
5.exit
enter your choice:
1
enter name to be add:
nandini
enter the position after which position u want to add:
0
*****MENU*****
1.add record at given position
2.delete record at given position
3.update record
4.display all records
5.exit
enter your choice:
1
enter name to be add:
ramadevi
enter the position after which position u want to add:
1
*****MENU*****
1.add record at given position
2.delete record at given position
3.update record
4.display all records
5.exit
enter your choice:
1
enter name to be add:
ravinder
enter the position after which position u want to add:
2
*****MENU*****
1.add record at given position
2.delete record at given position
3.update record
4.display all records
5.exit
enter your choice:
4
no.of records:4
	**********LIST*********
		nandini
		ramadevi
		ravinder
		naveena
*****MENU*****
1.add record at given position
2.delete record at given position
3.update record
4.display all records
5.exit
enter your choice:
2
enter the position:
0
cannot delete the record at position:0
*****MENU*****
1.add record at given position
2.delete record at given position
3.update record
4.display all records
5.exit
enter your choice:
1
enter name to be add:
prashanti
enter the position after which position u want to add:
3
*****MENU*****
1.add record at given position
2.delete record at given position
3.update record
4.display all records
5.exit
enter your choice:
4
no.of records:5
	**********LIST*********
		nandini
		ramadevi
		ravinder
		prashanti
		naveena
*****MENU*****
1.add record at given position
2.delete record at given position
3.update record
4.display all records
5.exit
enter your choice:
2
enter the position:
4
*****MENU*****
1.add record at given position
2.delete record at given position
3.update record
4.display all records
5.exit
enter your choice:
4
no.of records:4
	**********LIST*********
		nandini
		ramadevi
		ravinder
		naveena
*****MENU*****
1.add record at given position
2.delete record at given position
3.update record
4.display all records
5.exit
enter your choice:
3
no.of records:4
	**********LIST*********
		nandini
		ramadevi
		ravinder
		naveena
enter position at which you want to update the record:
4
enter the new name to update:
prashanti
successfully updated the name at position:4
*****MENU*****
1.add record at given position
2.delete record at given position
3.update record
4.display all records
5.exit
enter your choice:
5
****************************************************************************/

