/*----------------------------------------------------------------------------------------------------------------------------------
AUTHOR:
-------
  TATAJI.IRRINKULA	Emp_Id-10347	email: tatajiirrinki@gmail.com		Cell : 9704147867


  PURPOSE:
  ---------
  	This program is to find the given number is palindrome or not without using arrays  ----------------------------------------------------------------------------------------------------------------------------------*/

#include<stdio.h>
#include<math.h>

//count_No_Of_Digits function definiton  return's the no.of digits present in the given number

int countNoOfDigits(int number) {
	int no_of_digits=0,temp=number;
	while(temp%10) {
		no_of_digits++;
		temp=temp/10;
	}
	return no_of_digits;
}

//isPalindrome function definition to checks whether the given number is palindrome or not

int isPalindrome(int number) {

	int no_of_digits,temp=number,firstTerm,secondTerm,count=0;
	// calling count_No_Of_Digits
	no_of_digits=countNoOfDigits(number);

	firstTerm=temp%10;
	secondTerm=temp/pow(10,no_of_digits-1);

	while( firstTerm == secondTerm ) {
		count++;
		if(count==no_of_digits/2) {
			return 1;
		}
	firstTerm=temp%10;
	secondTerm=temp/pow(10,no_of_digits-1);

	}
	return 0;
}

int main() {
	
	int number,flag=0;
	
	printf("enter the number to check given number is palindrome on not\n");
	scanf("%d",&number);
	
	//calling the isPalindrome function
	flag=isPalindrome(number);

	if(flag) {
		printf("entered number  %d is palindrome \n",number);
	} else {
		printf("entered num is %d not palindrome\n",number);
	}
	
	return 0;
}
/**********************************************************************************************
 OUTPUT:
-------
	enter the number to check given number is palindrome on not
12365421
entered number  12365421 is palindrome 
[fedora@localhost Downloads]$ ./a.out
enter the number to check given number is palindrome on not
1256521
entered number  1256521 is palindrome 	


**********************************************************************************************/

