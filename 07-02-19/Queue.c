/******************************************************************************************
 AUTHOR:
--------
	TATAJI.IRRINKULA    Emp_Id-10347   email: tatajiirrinki@gmail.com  9704147867
	
	Purpose:
	--------
		In this program we perform circuar Queue by using linked list and perform 
		operations like enque,deque and dispaly the data present in the list 

******************************************************************************************/
#include<stdio.h>
#include<stdlib.h>
#define SIZE 8 
int count;
struct node display(struct node *);
struct node *enqueue(struct node *);
void dequeue(struct node **);
struct node *search(struct node *,int);


struct node {
	int value;
	struct node *next;
};


main() {
	int data;
	struct node *head=NULL;
	struct node *temp=NULL;
	while(1) {
		int choice;
		printf("1.enque  2.display 3.search 4.deque 5.exit\n");
		printf("enter your choice\n");
		scanf("%d",&choice);
		switch(choice) {
			case 1: head=enqueue(head);
				break;
			case 2:display(head);
			       break;
			case 3:printf("enter the data to search\n");
			       scanf("%d",&data);
			       temp=search(head,data);
			       if(temp==0) {
				       printf("the data is not found\n"); }
			       break;
			case 4: dequeue(&head);
				break;
			case 5:exit(0);
			       break;
			default:printf("enter your correct choice\n");
		}
	}
}

//add the student details at the end
struct node *enqueue(struct node *ptr) {
	struct node *nu=NULL,*temp=NULL;
	nu=calloc(1,sizeof(struct node));
	if(nu==NULL) {
		printf("Error memory allocation is not done\n");
		return;
	}

	if(count==SIZE) {
		printf("Queue is full\n");
		return;
	}

	printf("enter the value\n");
	scanf("%d",&nu->value);
	//printf("enter the name\n");
	//scanf("%s",nu->name);
	//printf("enter the percentage\n");
	//scanf("%f",&nu->per);


	if(ptr==NULL) {
		count++;
		ptr=nu;
		ptr->next = nu;
	}

	else {
		count++;
		temp=ptr;
		while(temp->next!=ptr) {
			temp=temp->next;
		}
		temp->next=nu;
		nu->next = ptr;
	}

	return ptr;
}

//deleting the student details at the first
void dequeue(struct node **ptr) {
	struct node *temp=NULL;
	struct node *temp1=NULL;

	if(*ptr==NULL) {
		printf("Queue is empty\n");		
	}

	else if(count==1 ) {
		free(*ptr);
		*ptr=NULL;
		count--;
	}
	else {
		temp=*ptr;
		*ptr=(*ptr)->next;
		for(temp1=*ptr;temp1->next!=temp;temp1=temp1->next);
		temp1->next=*ptr;
		free(temp);
		temp=NULL;
		count--;
	}
}

//displaying the student details

struct node display(struct node *ptr) {
	struct node *temp=NULL;
	temp=ptr;
	if(temp==NULL) {
		printf("Queue is empty\n");
		return;
	}

do {
printf("%d  \n",temp->value);
temp=temp->next;
} while(temp!=ptr);
}

//searching the student details based upon roll number

struct node *search(struct node *ptr, int d) {
struct node *nu=NULL;
if(ptr==NULL) {
printf("the sll is empty");
return ptr;
}

while(ptr) {
if(ptr->value==d) {
printf("the details are");
printf("%d ",ptr->value);
}
ptr=ptr->next;
}

}
/******************************************************************************************
 OUTPUT:
--------
1.enque  2.display 3.search 4.deque 5.exit
enter your choice
1
enter the value
2
1.enque  2.display 3.search 4.deque 5.exit
enter your choice
1
enter the value
3
1.enque  2.display 3.search 4.deque 5.exit
enter your choice
1
enter the value
4
1.enque  2.display 3.search 4.deque 5.exit
enter your choice
1
Queue is full
1.enque  2.display 3.search 4.deque 5.exit
enter your choice
4
Segmentation fault (core dumped)
[fedora@localhost Downloads]$ vi Queue.c 
[fedora@localhost Downloads]$ gcc Queue.c 
[fedora@localhost Downloads]$ ./a.out
1.enque  2.display 3.search 4.deque 5.exit
enter your choice
1
enter the value
1
1.enque  2.display 3.search 4.deque 5.exit
enter your choice
1
enter the value
2
1.enque  2.display 3.search 4.deque 5.exit
enter your choice
1
enter the value
3
1.enque  2.display 3.search 4.deque 5.exit
enter your choice
1
enter the value
4
1.enque  2.display 3.search 4.deque 5.exit
enter your choice
2
1  
2  
3  
4  
1.enque  2.display 3.search 4.deque 5.exit
enter your choice
4
1.enque  2.display 3.search 4.deque 5.exit
enter your choice
2
2  
3  
4
************************************************************************************/
