/*********************************************************************************
  AUTHOR:
  ------- 

	TATAJI.IRRINKULA   Emp-Id:10347	   tatajiirrinki@gmail.com     9704147867
	Purpose;
	-------
	This program is to find roots of a quadratic equation

 **************************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<math.h>

int main() {

	int a,b,c;//These are the coefficients for variables and constant
	float root1,root2,exp1,exp2;//these are instant variables for store root values

	printf("enter a,b and c values\n");
	scanf("%d%d%d",&a,&b,&c);

	root1= -b;
	root2=(b*b-(4*a*c));

	//condition for imaginary roots
	if(root2<0) {
		root2 = -root2;
		exp1= root1/(2*a);
		exp2 = sqrt(root2)/(2*a);
		printf("the two roots are :%f+i%f\n and %f-i%f\n",exp1,exp2,exp1,exp2);
	}

	//condition for real roots
	else {
		exp1 = (root1+sqrt(root2))/(2*a);
		exp2= (root1-sqrt(root2))/(2*a);
		printf("the two roots are :%f\n and %f\n",exp1,exp2);
	}

	return 0;
}

/********************************************************
OUTPUT:
-------
	enter a,b and c values
1
2
3
the two roots are :-1.000000+i1.414214
 and -1.000000-i1.414214
 

 *****************************************************/
