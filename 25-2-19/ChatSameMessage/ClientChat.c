/*********************************************************************************************************************************************
AUTHOR:
-------
	TATAJI IRRINKULA 	tatajiirrinki@gmail.com	    Emp-Id:10347 	Ph.No:9704147867
	Purpose:
	-------
	This program is for server client chating application. When server starts the process, the client will send the message. Then in 	response to the client, the server sends the same message to the client. 
*****************************************************************************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<netdb.h>

//this function is to print the error message
void error(char *msg){
	perror(msg);
	exit(1);
}

int main(int argc, char **argv){
	//initialising the instant variables.
	int sockfd,portNum,ret;
	char buf[1024];
	//These are pre-defined structures for socket programming
	struct hostent *server;
	struct sockaddr_in serv_addr;
	int n;
	if(argc<3){
		fprintf(stderr,"usage %s hostname port\n",argv[0]);
		exit(0);
	}
	//collecting the portnum from server
	portNum = atoi(argv[2]);
	sockfd = socket(AF_INET,SOCK_STREAM,0);
	if(sockfd<0)error("Error openeing socket");
	server = gethostbyname(argv[1]);
	if(server == NULL){
		fprintf(stderr,"error,no such host\n");
		exit(0);
	}
	memset((char*) &serv_addr,0,sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	strncpy((char *)server->h_addr,(char *)&serv_addr.sin_addr.s_addr,server->h_length);
	serv_addr.sin_port = htons(portNum);
	if(connect(sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr))<0)
		error("ERROR IN CONNECTING");
	
	//calling the fork and creating the two process
	ret = fork();
	//child process
	if(ret == 0) {
		while(1) {
	       memset(buf,0,sizeof(buf));
	       fgets(buf,1024,stdin);
	       write(sockfd,buf,strlen(buf));
		}
	}
	//parent process
	else {
		while(1) {
	       memset(buf,0,sizeof(buf));	    
   		read(sockfd,buf,1024);
	       printf("From server:%s\n",buf);
		}
	}

	//closing the socket file descriptor			
       close(sockfd);
return 0;
}

/***************************************************************************************************************************************
OUTPUT:
-------

 ./a.out localhost 12345
hai
From server:hai

Hello Innominds
From server:Hello Innominds

****************************************************************************************************************************************/
