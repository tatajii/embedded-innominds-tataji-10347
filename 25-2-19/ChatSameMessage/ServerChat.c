/*********************************************************************************************************************************************
AUTHOR:
-------
	TATAJI IRRINKULA	   tatajiirrinki@gmail.com	    Emp-Id:10347 	Ph.No:9704147867
	Purpose:
	-------
	This program is for server client chating application. When server starts the process, the client will send the message. Then in 	response to the client, the server sends the same message to the client. 
*****************************************************************************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<netdb.h>

int tempfd=0;

//this function is to print the error message
void error(char *msg){
	perror(msg);
	exit(1);
}

int main(int argc, char **argv){
	//initialising the instant variables.
	int sockfd,newSockfd,portNum,ret;
	char buf[1024];
	//These are pre-defined structures for socket programming
	struct sockaddr_in serv_addr, cli_addr;
	int n;
	socklen_t cliLen;
	if(argc<2){
		fprintf(stderr,"ERROR, no port available\n");
		exit(1);
	}
	sockfd = socket(AF_INET,SOCK_STREAM,0);
	if(sockfd<0)error("Error openeing socket");
	memset((char*) &serv_addr,0,sizeof(serv_addr));
	portNum = atoi(argv[1]);
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(portNum);
	if(bind(sockfd,(struct sockaddr*) &serv_addr, sizeof(serv_addr))<0)
		error("ERROR ON BINDING");
	listen(sockfd,5);
       while(1){
	    //calling the fork and creating the two process
	       ret = fork();
	       if(ret == 0) {
	       memset(buf,0,sizeof(buf));
	       read(newSockfd,buf,1024);
	       printf("%s\n",buf);

	     //writing onto the newSockfd from buffer
	       write(newSockfd,buf,strlen(buf));
	       }
	       else {
	cliLen = sizeof(cli_addr);
	newSockfd = accept(sockfd,(struct sockaddr *) &cli_addr,&cliLen);
	if(newSockfd<0)error("ERROR IN ACCEPTING");
	       }       
       }
       close(newSockfd);
       close(sockfd);
return 0;
}


/**************************************************************************************************************************

OUTPUT:
-------
./a.out 12345
hai

Hello Innominds

****************************************************************************************************************************/
