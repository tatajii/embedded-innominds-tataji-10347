/******************************************************************************************************************************************
AUTHOR:
-------
Tataji Irrinkula    tatajiirrinki@gmail.com	 9704147867	Emp_Id-10347

Purpose:
--------
	This program is the implementation of user defined shell(grep is not implemented).

******************************************************************************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/stat.h>

char *arguements[100];

void readFromCommandUser() {
	char *str,*token;
	int i=0;
	str = (char *)malloc(20);
	gets(str);
	token=strtok(str," ");
	while(token!=NULL) {
		arguements[i++]=token;
		token=strtok(NULL," ");
	}
	arguements[i]=token;
	//return str;

}

int main() {
	int ret=-1,child_exit_value;
	char cwd[1024];
	char *username;

	getlogin_r(username,10);
	username[strlen(username)]='@';
	char name[] = "myShell$";
	strcat(username,name);

	while(1) {

		write(1,username,strlen(username));
		readFromCommandUser();
		
		if(!(strcmp(arguements[0],"exit"))) {
			exit(0);
		}

		if(!(strcmp(arguements[0],"pwd"))) {
			getcwd(cwd,sizeof(cwd));
			write(1,cwd,strlen(cwd));
			printf("\n");
			continue;
		}

		ret = fork();

		if(ret==0) {
			execvp(arguements[0],arguements);
		}
		else {
			wait(&child_exit_value);
		}
	}

		return 0;

}

/****************************************************************************************************************************************
OUTPUT:
-------
busam@myShell$pwd
/home/busam/tataji
busam@myShell$exit
busam@busam-Lenovo-G505:~/bhaskar$ ./a.out
busam@myShell$pwd
/home/busam/tataji
busam@myShell$ls
a.out  Array1.c  Array.c  ConvolutionOfMatrix.c  shell.c  test1.c
busam@myShell$ls 
a.out  Array1.c  Array.c  ConvolutionOfMatrix.c  shell.c  test1.c
busam@myShell$man fgets
busam@myShell$exit



***************************************************************************************************************************************/

