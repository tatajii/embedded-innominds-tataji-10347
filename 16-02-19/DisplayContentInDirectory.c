/****************************************************************************************
 AUTHOR:
 -------
	TATAJI.IRRINKULA  Emp_ID-10347  email: tatajiirrinki@gmail.com  9704147867

	Purpose:
	-------
		This program is used to display the files in the directory.


******************************************************************************************/
#include<stdio.h>
#include <sys/types.h>
#include <dirent.h>
int main(int argc,char**argv){
	DIR *dp;
	struct dirent *ptr;
	dp = opendir(argv[1]);
	while(ptr=readdir(dp)){
		printf("%s\t",ptr->d_name);
	//	printf("%d\n",ptr->d_ino);
	}
	closedir(dp);
	return 0;

}
/*****************************************output of the above program***********************************************************************
 SumOfTwoValuePair.c	..	hello.c	header.h	test	const1.c	makefile	6-02-2019	2dusingsinglepointer.c	MatrixConvolution.c	test.c	ConvolutionMatrix.c	ConvolutionOfMatrix.c	ll.c	MultiplicationOfTwoMatrices.c	MaximumValueinTheSubarray.c	DoublePointers.c	Chocolate.c	temp	.	header.h.gch	11-2-2019	test.s	MultiplicationOfMatrices.c	MaximumSumValueInSubarray.c	intarr.c	pointers.o	pointers.c	test.o	strtok1.c	test.exe	SinglePointer.c	MaxvalueInTheSubarray.c	point.c	hello1	19-2-2019	array.c	test1.c	test.i	str.c	hello1.c7-2-2019pointertostruct	dynamicallocation.c	Calculator	func.c
 ********************************************************************************************************************************************/
