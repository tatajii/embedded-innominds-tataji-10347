/*----------------------------------------------------------------------------------------------------------------------------------
AUTHOR:
------
  TATAJI.IRRINKULA	     Emp_Id-10347	tatajiirrinki@gmail.com		Cell : 9704147867

PUROPOSE:
---------
      In this program we perform the multiplication of two matrices  only if columns of first matrix equal to 
    rows of second matrix.
  NOTE: Here we generate random matrix by using rand function.
  ------------------------------------------------------------------------------------------------------------------------------------*/

#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>

//display eleemts in the matrix
void displayMatrix(int rows,int columns,int matrix[][columns]) {

	for(int i=0;i<rows;i++){
		for(int j=0;j<columns;j++){
			printf("%d ",matrix[i][j]);
		}
		printf("\n");
	}
}

int main() {

	int row1,column1,sum,row2,column2;


	printf("enter no.of rows for matrix1 \n");
	scanf("%d",&row1);
	while(row1<=0) {
		printf("in a matrix rows cannot be negative or 0,please re enter \n");
		scanf("%d",&row1);
	}
	printf("enter no.of columns for matrix1 \n");
	scanf("%d",&column1);
	while(column1<=0) {
		printf("in a matrix column cannot be negative or 0,please re enter \n");
		scanf("%d",&column1);
	}

	printf("enter no.of rows for matrix2 \n");
	scanf("%d",&row2);
	while(row2<=0) {
		printf("in a matrix rows cannot be negative or 0,please re enter \n");
		scanf("%d",&row2);
	}
	printf("enter no.of columns for matrix2 \n");
	scanf("%d",&column2);
	while(column2<=0) {
		printf("in a matrix column cannot be negative or 0,please re enter \n");
		scanf("%d",&column2);
	}


	if(column1!=row2) {

		printf("matrix multiplicaton is not possible \n");
		exit(0);
	}

	srand(getpid());
	int matrix1[row1][column1];
	int matrix2[row2][column2];
	int resultMatrix[row1][column2];

	for(int i=0;i<row1;i++){
		for(int j=0;j<column1;j++){
			matrix1[i][j]=rand()%10;
		}
	}
	printf(" data in the first matrix is\n");

	displayMatrix(row1,column1,matrix1);

	for(int i=0;i<row2;i++){
		for(int j=0;j<column2;j++){
			matrix2[i][j]=rand()%10;
		}
	}
	printf("data in the second matrix is\n");

	displayMatrix(row2,column2,matrix2);

	for(int i=0;i<row2;i++){
		for(int j=0;j<column2;j++){
			resultMatrix[i][j]=0;
			for(int k=0;k<column1;k++){
				resultMatrix[i][j]+=matrix1[i][k]*matrix2[k][j];
			}
			
		}
	}

	printf(" the resultant matrix after multiplication\n");
	displayMatrix(row1,column2,resultMatrix);

	return 0;	
}

/**************************************************************************************
  OUTPUT:
  ------
  enter no.of rows for matrix1
2
enter no.of columns for matrix1
3
enter no.of rows for matrix2
3
enter no.of columns for matrix2
4
printing the first matrix
5 5 0
2 6 2
printing the second matrix
0 9 3 1
6 8 0 8
0 0 0 6
printing the resultant matrix after multiplication
30 85 15 45
36 66 6 62  
 ************************************************************************************************/