/*----------------------------------------------------------------------------------------------------------------------------------
    AUTHOR:
    ------
        TATAJI.IRRINKULA	Emp_Id-10347	email: tatajiirrinki@gmail.com		Cell : 9704147867


  PURPOSE
  ---------
    This program is to find the number of trailing zeros at the end of the factorial of given number
  ----------------------------------------------------------------------------------------------------------------------------------*/


#include<stdio.h>
#include<math.h>


int main() {
	int number,result=0,n=1;
	printf("enter the number to find trailing zeros at the end of the factorial  \n");
	scanf("%d",&number);
	
	while(number>=pow(5,n)) {
		result=result+(number/pow(5,n));
		
		n++;
	}
	printf("the no of trailing zeros at the end of  %d factorial is %d \n",number,result);
	return 0;
}
/**************************************************************************
 OUTPUT:
 ------
 enter the number to find trailing zeros at the end of the factorial 

233

the number to  trailing zeros at the end of  233 factorialis 56
********************************************************/