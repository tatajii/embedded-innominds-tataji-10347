#include<stdio.h>
#include<unistd.h>
#include<strings.h>
#include<string.h>
#include<stdlib.h>
#include<sys/types.h>
#include<netdb.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<netinet/ip.h>
#include<pthread.h>

int cfd;
char buf[256]={'\0'};

void *writeFunc(void *b) {
	//reading and writing the data through rwad and write system calls
	while(1) {
		memset(buf,0,sizeof(buf));
		gets(buf);
		write(cfd,buf,strlen(buf));
	}

}

void *readFunc(void *b) {
	while(1) {
		read(cfd,buf,255);
		printf("msg:%s\n",buf);
		memset(buf,0,sizeof(buf));
	}

}

int main(int argc,char* argv[]) {

	//checking whether the command line arguments are sufficient or not
	if(argc<3) {
		perror("insufficient command line arguments\n");
		exit(1);
	}

	//declaring the local variables
	struct sockaddr_in ser_addr;
	struct hostent *server;
	pthread_t id1,id2;

	//creating the client socket through socket system call
	cfd=socket(AF_INET,SOCK_STREAM,0);
	//displaying the error message if it fails
	if(cfd<0) {
		printf("ERROR:failed in opening the socket\n");
		exit(1);
	}

	//getting the server details through gethostbyname system call
	server=gethostbyname(argv[1]);

	//initializing the members of sockaddr_in structure
	ser_addr.sin_family=AF_INET;
	ser_addr.sin_port=htons(atoi(argv[2]));
	//ser_addr.sin_addr.s_addr=INADDR_ANY;

	bcopy((char*)server->h_addr,(char*)&ser_addr.sin_addr.s_addr,server->h_length);

	//connecting the client socket to server through connect system call
	connect(cfd,(struct sockaddr *)&ser_addr,sizeof(ser_addr));

	pthread_create(&id1,NULL,writeFunc,NULL);
	pthread_create(&id2,NULL,readFunc,NULL);

	pthread_join(id1,NULL);
	pthread_join(id2,NULL);

	//returning from main
	return 0;

}

