#include<stdio.h>
#include<stdio_ext.h>
#include<unistd.h>
#include<string.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<netinet/ip.h>
#include<pthread.h>

int cfd,cfd2;
char buf[256]={'\0'};
void *readWrite(void *b) {

while(1) {

			read(cfd,buf,255);
			
			//printf("\tfrom client:%s\n",buf);
			write(cfd2,buf,strlen(buf));
			memset(buf,'\0',sizeof(buf));
			
		}


}

void *writeRead(void *b) {

while(1) {
			read(cfd2,buf,255);
			//printf("\tfrom client:%s\n",buf);
			write(cfd,buf,strlen(buf));
			memset(buf,'\0',sizeof(buf));

		}

}

int main(int argc,char* argv[]) {

	//checking whether the command line arguments are sufficient or not
	if(argc<2) {
		perror("insufficient command line arguments\n");
		exit(1);
	}

	//declaring the local variables
	int sfd;
	
	struct sockaddr_in ser_addr,cli_addr;
	socklen_t len;
	pthread_t id1,id2;

	//creating the server socket
	sfd=socket(AF_INET,SOCK_STREAM,0);

	//if it fails printing the error message
	if(sfd<0) {
		printf("ERROR:failed in opening the socket\n");
		exit(1);
	}

	//initializing the variables of structure sockaddr_in 
	ser_addr.sin_family=AF_INET;
	ser_addr.sin_port=htons(atoi(argv[1]));
	ser_addr.sin_addr.s_addr=INADDR_ANY;

	//binding the server socket if it fails printing the error message
	if(bind(sfd,(struct sockaddr *)&ser_addr,sizeof(ser_addr))==-1) {
		perror("ERROR:binding failed\n");
		exit(1);
	}

	//listen to client requests through listen call if it fails printing the error message
	if(listen(sfd,1)<0) {
		printf("ERROR:Listen call fails\n");
		exit(0);
	}

	//accepting the client's request through accept call
	len=sizeof(cli_addr);
	cfd=accept(sfd,(struct sockaddr *)&cli_addr,&len);
	//if it fails displaying the error message
	if(cfd<0) {
		perror("ERROR:accept failed\n");
		exit(1);
	}	
	//if it fails displaying the error message
	cfd2=accept(sfd,(struct sockaddr *)&cli_addr,&len);
	if(cfd2<0) {
		perror("ERROR:accept failed\n");
		exit(1);
	}	

	//reading and writing the data onto socket through read and write calls
	pthread_create(&id1,NULL,readWrite,NULL);
	pthread_create(&id2,NULL,writeRead,NULL);

	pthread_join(id1,NULL);
	pthread_join(id2,NULL);

	//returning from main
	return 0;

}
