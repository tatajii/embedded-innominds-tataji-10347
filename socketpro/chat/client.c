// header files
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<netdb.h>

// main function
int main(int argc, char **argv) {
	// initialising variables
	int sockfd,portNum,n;
	char buff[1024];
	//initailising predefined structure variables
	struct hostent *server;
	struct sockaddr_in serv_addr;
	// checking arguements count
	if (argc<3) {
		printf("Invalid arguements, provide ip address and portnumber");
		exit(0);
	}
	//asigning arument2 to portnumber
	portNum=atoi(argv[2]);
	// creating new socket
	sockfd= socket(AF_INET,SOCK_STREAM,0);

	if(sockfd<0) {
		printf("Error opening socket");
		exit(0);
	}
	//taking server address into server
	server=gethostbyname(argv[1]);
	if (server==NULL) {
		printf("Error,no such host");
		exit(0);
	}
	// to avoid garbage set 0 to server address
	memset((char*)&serv_addr,0,sizeof(serv_addr));
	// assiging 
	serv_addr.sin_family=AF_INET;
	serv_addr.sin_addr.s_addr=INADDR_ANY;
	serv_addr.sin_port=htons(portNum);
	// connect to server
	if(connect(sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr))<0){
		printf("Error in connecting");
	}
	while(1) {
		
		memset(buff,0,sizeof(buff));
		//taking data from user
		fgets(buff,1024,stdin);
		// send to server
		n=write(sockfd,buff,strlen(buff)+1);
		if(n<0){
			printf("error on writing");
		
		}
		printf("client=%s\n",buff);
		memset(buff,0,sizeof(buff));
		// receive data from server
		n=read(sockfd,buff,1024);
		if(n<0){
			printf("error on reading");
		}
		printf("server = %s\n",buff);
		// when bye close connection
		int i=strncmp("bye",buff,3);
		if(i==0){
			break;
		}
	}
	close(sockfd);
	return 0;
}





