//header files 
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<netdb.h>

/* main function starts 
  in this main function int argc give number of arguments used in server
  argv gives arguments*/

int main(int argc,char **argv) {
	// intialisation of variables
	int sockfd,newsockfd,portNum,n;
	char buff[1024];
	//here intialising predefined structure variables
	struct sockaddr_in serv_addr,cli_addr;
	socklen_t cliLen;
	// checking arguements count 
	if (argc<2) {
	
		printf("invalid arguements");
		exit(0);
	}

	// creating a socket means communication end point
	sockfd=socket(AF_INET,SOCK_STREAM,0);
	// checking socket is created or not
	if(sockfd<0) {
		printf("Error in opening socket");
		exit(0);
	}
	//to avoid garbage fill with 0
	memset((char*)&serv_addr,0,sizeof(serv_addr));
	// taking port number and convert into integer
	portNum=atoi(argv[1]);
	//assigning values to server address member
	serv_addr.sin_family=AF_INET;
	serv_addr.sin_addr.s_addr=INADDR_ANY;
	serv_addr.sin_port=htons(portNum);
	//assign address to socket and checking
	if(bind(sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr))<0){
		printf("error on binding");
	}
	// willingness to connect
	listen(sockfd,5);
	// taking clilen
	cliLen=sizeof(cli_addr);
	//accept the connection
	newsockfd=accept(sockfd,(struct sockaddr *)&cli_addr,&cliLen);
	if(newsockfd<0) {
		printf("error on accepting");
		exit(0);
	}
	 while(1) {
		 // clear buff
                bzero(buff,1024);
		// receiving the client data
                n=read(newsockfd,buff,1024);
                if(n<0){
                        printf("error on reading");
                }
                printf("cient= %s\n",buff);
		//set buff with NULL
                memset(buff,0,sizeof(buff));
		//taking input from user
                fgets(buff,1024,stdin);
		//send the server data
                n=write(newsockfd,buff,strlen(buff)+1);
                if(n<0){
                        printf("error on writing");
                }
                printf("server= %s\n",buff);
		// when type bye close connection
                int i=strncmp("bye",buff,3);
                if(i==0){
                        break;
                }
        }
        close(sockfd);
        return 0;
}


