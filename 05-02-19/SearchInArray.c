/**************************************************************************************
 AUTHOR:
 ------
    TATAJI.IRRINKULA     Emp-Id-10347    mail:tatajiirrinki@gmail.com   9704147867
    
    Purpose:
        In this program , claclutae the sum of two numbers present in the array is equal to search elemente .
        if present print the indexes of elements in an array. 
    
***************************************************************************************/    
#include <stdio.h>
#include<stdlib.h>
// declaring array function
int *getArray (int);
// declaring getData funtion 
int *getData (int *, int, int);
// main funtion
int main () {
  
    int size, num;
  
    int *search = NULL;
  
    printf ("enter the size of the array:");
  
    scanf ("%d", &size);
   // calling getArray function to get array elements
    int *array = getArray (size);
  
    printf ("\nenter the search element in the array:");
  
    scanf ("%d", &num);
  // calling getData function to search number is present or not
    search = getData (array, size, num);
  
    for (int i = 0; i < 2; i++) {
      
        printf ("the indexes are :%d\n", search[i]);
    
    } 
    return 0;

}

// definition of getArray function
int *getArray (int size) {
  
    int *array = (int *) malloc (size * sizeof (int));
  
    printf ("enter the arrray elements:");
  
    for (int i = 0; i < size; i++) {
      
 
        scanf ("%d", &array[i]);
    } 
    printf ("the array elements are:");
  
    for (int i = 0; i < size; i++) {
      
    printf ("%d ", array[i]);
    
    } 
    return array;

}


 // definition of getData function
int *getData (int *array, int size, int num) {
  
 
    int *getData = NULL;
  
    int i, j,temp=num;
  
    getData = (int *) malloc (2 * (sizeof (int)));
  
    for (i = 0, j = size - 1; i <= j; i++, j--) {
        
            temp = array[i] + array[j];
              
        
      
         if (temp < num) {
	  
            i++;
            
        }
      
        else if (temp > num) {
	  
            i--;
            
        }
      
        else {
            
	        getData[0] = i;
	  
            getData[1] = j;
	  
            return getData; 
        
        }
    
        }

}

/*************************************************************************************
 OUTPUT:
 ------
 
 enter the size of the array:5
 enter the arrray elements: 1
 4
6
8
9
the array elements are:1 4 6 8 9
enter the search element in the array:14
the indexes are :2
the indexes are :3
*************************************************************************************/