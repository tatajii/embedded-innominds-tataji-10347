
#include<stdio.h> 
//this function finds the fibonacci series for the given number
int Fibo(int n)
{
	if ( n == 0 )
		return 0;
	else if ( n == 1 )
		return 1;
	else
		return ( Fibo(n-1) + Fibo(n-2) );
} 

int main () 
{ 

	int num,i=0;
	printf("enter the number to find the fibonacci series :\n");
	scanf("%d",&num);
	while(num<0) {
		printf("factorial cannot be negative......re enter \n");
		scanf("%d",&num);
	}

	printf("Fibonacci series for the given number is \n");

	for (int j = 1 ; j <= num ; j++ )
	{
		printf("%d ", Fibo(i));
		i++; 
	}
	printf("\n");

	return 0;
}

