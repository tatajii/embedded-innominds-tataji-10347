#include<stdio.h>

int main() {
	int var1,var2,temp,temp1,temp2,hcf,lcm;
	printf("enter the two integers var1 and var2");
	scanf("%d%d",&var1,&var2);

	temp1=var1;
	temp2=var2;
	while(temp2!=0) {
		temp=temp2;
		temp2=temp1%temp2;
		temp1=temp;
	}
	hcf=temp1;
	lcm=(var1*var2)/hcf;
	printf("the hcf of two numbers %d and %d is %d",var1,var2,hcf);
	printf("the lcm of two numbers %d and %d is %d",var1,var2,lcm);
	return 0;
}
