#include<stdio.h>
#include<string.h>

int anagram(char*s1, char*s2) {
	int array1[26]={0},array2[26]={0};
	if(strlen(s1)!=strlen(s2)) 
		return 0;
	for(int i=0;i<strlen(s1);i++)
			array1[s1[i]-'a']++;
	
	for(int i=0;i<strlen(s2);i++)
			array2[s2[i]-'a']++;
	for(int i=0;i<26;i++) {
		if(array1[i]!=array2[i])
			return 0;
	}
	return 1;
}

int main() {
	char s1[20],s2[20];
	printf("enter the string1:\n");
	scanf("%s",s1);
	printf("enter the string2:\n");
	scanf("%s",s2);

	if(anagram(s1,s2)) {
			printf("strings are anagrams to each other\n");
		}
	else

			printf("strings are not anagrams to each other\n");
	return 0;
}
