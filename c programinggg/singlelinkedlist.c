#include<stdio.h>
#include<stdlib.h>


struct node {
	int data;
	struct node *next;
};

struct node *head=NULL;
void insertatBegin(int);
void insertatEnd(int);
void traverse();
void deletefromBegin();
void deletefromEnd();
int count=0;


    int main () {
       int input, data;

       for (;;) {
          printf("1. Insert an element at beginning of linked list.\n");
          printf("2. Insert an element at end of linked list.\n");
          printf("3. Traverse linked list.\n");
          printf("4. Delete element from beginning.\n");
          printf("5. Delete element from end.\n");
          printf("6. Exit\n");

          scanf("%d", &input);
	  if(input==1){
		  printf("Enter the data \n");
		  scanf("%d",&data);
		  insertatBegin(data);
	  }
	  else if(input==2) {
		  printf("Enter the data \n");
		  scanf("%d",&data);
		  insertatEnd(data);
	  }
	  else if(input==3) {
		  traverse();
	  }
/*	  else if(input==4) {
		  deletefromBegin();
	  }
	  else if(input==5) {
		  deletefromEnd();
	  }*/
	  else if(input==6) {
		  break;
	  }
	  else
		  printf("Enter the valid input: \n");
       }
       return 0;
    }
void insertatBegin(int x) {
	struct node *temp;
	temp=(struct node*)calloc(1,sizeof(struct node));
	temp->data=x;
	count++;
	if(head==NULL) {
		head=temp;
		return;
	}
	temp->next=head;
	head=temp;
	return ;
}

void insertatEnd(int x) {
	struct node *temp,*temp2;
	temp=(struct node*) calloc(1,sizeof(struct node));
	count++; 
	temp->data=x;
	if(head==NULL) {
		head=temp;
		return;
	}
	temp2=head;
	while(temp2->next!=NULL)
		temp2=temp2->next;
	temp2->next=temp;
}
void traverse() {
	struct node* temp;
	temp=head;
	if(temp==NULL) {
		printf("list is empty\n");
		return;
	}
	printf("there are %d elements in the list.\n",count);

	while(temp->next!=NULL) {
		printf("%d  ",temp->data);
		temp=temp->next;;
	}
	printf("%d\n",temp->data);
}
void deletefromBegin() {
	struct node * temp;
	int n;
	if(head==NULL) {
		printf("list is empty:\n") ;
		return;
	}
	n=head->data;
	temp=head->next;
	free(head);
	head=temp;
	count--;
	printf("%d delete from begining successfully.\n",n);
}
void deletefromEnd() {

