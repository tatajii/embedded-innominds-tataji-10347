 #include<stdio.h>
#include<stdlib.h>

void swap(int*a,int*b) {
	int temp;
	temp=*b;
	*b=*a;
	*a=temp;
	return;
}
int * asort(int *a,int n) {
	int low=0,mid=0,high=n-1;
	while(mid<=high) {
		switch(a[mid]) {
			case 0:swap(&a[low],&a[mid]);
			       low++;
			       mid++;
			       break;
			case 1:mid++;
			       break;
			case 2:swap(&a[mid],&a[high]);
			       high--;
			       break;
		}
	}
	return a;
}
int main() {
	int n;
	printf("enter number of array elements: \n");
	scanf("%d",&n);
	int *a=(int*)malloc(sizeof(int)*n);
	printf("enter the arry elements:\n");
	for(int i=0;i<n;i++) {
		scanf("%d",&a[i]);
	}
	a=asort(a,n);
	printf("after being sorted...........\n");
	for (int i=0;i<=n-1;i++)
		printf("%d",a[i]);
	return 0;
}
