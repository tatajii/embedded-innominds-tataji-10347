#include<stdio.h>
#include<string.h>
#include<stdlib.h>
int sortArray(int *arr, int n) {
        int i,j;
        for(i=0;i<n;i++) {
		for(j=0;j<(n-1-i);j++) {
		if(arr[j]>arr[j+1]) {
			int temp=arr[j+1];
			arr[j+1]=arr[j];
			arr[j]=temp;
		}
		}
	}
	return 0;
}
int binarySearch(int *arr,int low,int high,int element) {
	int mid=(high+low)/2;
	if (mid<low)
		return -1;
	if (arr[mid]==element){
		return mid;
	}
	else if(arr[mid]>element) {
		high=mid-1;
		binarySearch(arr,low,high,element);
				}
	else {
		low=mid+1;
		binarySearch(arr,low,high,element);
		}
}




int main() {
        int number,i,arr[20],element,index;

        printf("enter number of elements in array");
        scanf("%d",&number);

        printf("\nEnter array elements:");
       for(i=0;i<number;i++) {
                scanf("%d",&arr[i]);   
        }
        sortArray(arr,number);
	printf("\narray elements after sorting is:");
	for(i=0;i<number;i++){
		printf("%d  ",arr[i]);
	}
	printf("\nenter the element to be searched:");
	scanf("%d",&element);
	index=binarySearch(arr,0,number-1,element);
	if(index>=0) {
		printf("element is present in the list at position:%d",index);
	}
	else {
		printf("elment is not present");
	}
	return 0;
}

