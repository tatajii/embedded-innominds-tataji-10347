#include<stdio.h>

int decimalToBinary(int number) {
	if(number==0) {
		return 0;	
	}
	else
		return(number%2+10*decimalToBinary(number/2));
}


int main() {
	int number;
	printf("Enter a number:");
	scanf("%d",&number);
	int a=decimalToBinary(number);
	printf("the binary  number is %d  for the given decimal number  %d\n",a,number);
	return 0;
}
