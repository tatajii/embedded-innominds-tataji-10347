#include <stdio.h>
#include <string.h>
#include <ctype.h>
 
int main()
{
    char string[100], word[20], unit[20], c;
    int i = 0, j = 0, count = 0;
 
    printf("Enter string: ");
    i = 0;
    gets(string);
    printf("Enter the word you want to find: ");
    scanf("%s", word);
    for (i = 0; i < strlen(string); i++)
    {
        while (i < strlen(string) && !isspace(string[i]))
        {
            unit[j++] = string[i++];
        }
        if (j != 0)
        {
            unit[j] = '\0';
            if (strcmp(unit, word) == 0)
            {
                count++;
            }
            j = 0;
        }
    }
 
    printf("The number of times the word '%s' found in '%s' is '%d'.\n", word, string, count);
}
