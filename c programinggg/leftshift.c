    #include <stdio.h>
     
    int main()
    {
       int n = 1, c, power;
       
       for ( c = 1 ; c <= 10 ; c++ )
       {  
          power = n << c;
          printf("2 raise to the power %d = %d\n", c, power);
       }
     
       return 0;
    }
