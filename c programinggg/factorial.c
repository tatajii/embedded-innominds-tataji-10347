#include <iostream>
using namespace std;
int calc(int res[],int n,int res_size)
{ int carry=0,i;
    for(i=0;i<res_size;i++)
    {
        int prod=res[i]*n+carry;
        res[i]=prod%10;
        carry=prod/10;
    }
    while(carry)
    {
        res[res_size]=carry%10;
        carry/=10;
        res_size++;
    }
    return res_size;
}
void fact(int n)
{
    int res[200],res_size=1,i;
    res[0]=1;
    for(i=2;i<=n;i++)
    res_size=calc(res,i,res_size);
    for(i=res_size-1;i>=0;i--)
    cout<<res[i];
}
int main() {
	int t ;
	cin>>t;
	while(t--)
	{
	    int n;
	    cin>>n;
	    fact(n);
	    cout<<"\n";
	}
	return 0;
}

