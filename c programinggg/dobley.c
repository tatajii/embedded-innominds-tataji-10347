#include<stdio.h>
#include<stdlib.h>

struct node {

	int data;
	struct node *prev,*next;
};

struct node* insert_at_beg(struct node *head,int data) {
	struct node* temp;
	temp=(struct node*)calloc(1,sizeof(struct node));
	temp->data=data;
	if(head==NULL) {
		head=temp;
		return head;
	}
	head->prev = temp;
	temp->next = head;
	head = temp;
	return head;
}

struct node* insert_at_end(struct node *head,int data) {
	struct node* temp;
	temp=(struct node*)calloc(1,sizeof(struct node));
	temp->data=data;
	if(head==NULL) {
		head=temp;
		return head;
	}
	struct node *ptr=head;
	for(;ptr->next!=NULL;ptr=ptr->next);

	ptr->next = temp;
	temp->prev = ptr;
	return head;
}

void display(struct node* head) {
	struct node *temp;
	if(head==NULL) {
		printf("empty.........\n");
	}
	for(temp=head;temp!=NULL;temp=temp->next) {
		printf("%d ",temp->data);
	}
	printf("\n");
}

struct node *delete_at_beg(struct node *head) {
	struct node *temp=head;
	if(head==NULL) {
		printf("empty......\n");
		return head;
	}
	if(head->next==NULL) {
		free(head);
		head=NULL;
		return head;
	}
	head=head->next;
	free(temp);
	temp=NULL;
	return head;
}

struct node *delete_at_end(struct node *head) {
	struct node *temp=head;
	if(head==NULL) {
		printf("empty..........\n");
		return head;
	}
	if(head->next==NULL) {
		free(head);
		head=NULL;
		return head;
	}
	struct node *ptr;
	for(;temp->next!=NULL;temp=temp->next);
	ptr=temp->prev;
	ptr->next=NULL;
	free(temp);
	temp=NULL;
	return head;
}

int main() {
	struct node *head=NULL;
	int choice,data;
	while(1) {
		printf("1.insert beg\n2.insert end\n3.display\n4.del beg\n5.del end\n6.exit");
		printf("enter choice:");
		scanf("%d",&choice);
		switch(choice) {

			case 1:
				printf("enter the data:");
				scanf("%d",&data);
				head=insert_at_beg(head,data);
				break;
			case 2:
				printf("enter the data:");
				scanf("%d",&data);
				head=insert_at_end(head,data);
				break;
			case 3:
				display(head);
				break;
			case 4:

				head=delete_at_beg(head);
				break;
			case 5:

				head=delete_at_end(head);
				break;
			case 6:
				exit(0);
			default:
				printf("default choice\n");

		}
}			
		return 0;
	}
