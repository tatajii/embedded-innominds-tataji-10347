#include<stdio.h>
//declare functions
 
float plus(float a, float b) { return a+b; }
float minus(float a, float b) { return a-b; }
float times(float a, float b) { return a*b; }
float divide(float a, float b) { return a/b; }
 
int main() {
 
//declare function pointers
 
  float(*fptr_p)(float, float);
  float(*fptr_m)(float, float);
  float(*fptr_t)(float, float);
  float(*fptr_d)(float, float);
 
//point ptr's to functions
 
  fptr_p=&plus;
  fptr_m=&minus;
  fptr_t=&times;
  fptr_d=&divide;
float result;
result=fptr_p(1,2);
printf("%f\n",result);
return 0;
}
