#include<stdio.h>
#include<stdlib.h>
#include<string.h>
int length;
struct student 
{
	int id;
	struct student *next;
};
struct student * addatBeginning(struct student *);
struct student * addatEnd(struct student *);
struct student * addatMiddle(struct student *);
struct student * deleteatBeginning(struct student *);
struct student * deleteatPosition(struct student *,int);
struct student * deleteatEnd(struct student *);
void menu(void);
void display(struct student *);
struct student * updateData(struct student *,int );
void search(struct student *,int );
int length=0;
int main()
{
	struct student *head=NULL;
	int choice,data,position;
	while(1)
	{
		menu();
		scanf("%d",&choice);
		switch(choice)
		{
			case 1: head=addatBeginning(head);
				length++;
		       		break;

			case 2: head=addatEnd(head);
				length++;
		       		break;
		
			case 3:head=addatMiddle(head);
			       length++;
		       		break;

			case 4: head=deleteatBeginning(head);
				length--;
		       		break;
	
			case 5: head=deleteatEnd(head);
				length--;
		       		break;
						
			case 6:
				printf("enter the position to be update :\n");
			       	scanf("%d",&position);
				head=deleteatPosition(head,position);
		       		break;

			case 7: display(head);
		       		break;

			case 8: printf("Enter the id u want to search:\n");
				scanf("%d",&data);
				search(head,data);
				break;
			case 9:printf("enter the position to be update :\n");
			       scanf("%d",&position);
			       head=updateData(head,position);
			       break;
		
			case 10:return 0;
		}
	}
	return 0;
}


void menu(void)
{
	printf("MENU:\n---------------\n1.Add Node at beginning\n2.Add Node at end\n3.Add  Node at middle\n4.Delete node at beginning\n5.Delete node at end\n6.Delete a node at respective position\n7.display all elements of list\n8.Search a node\n9.Update a node\n10.Quit\n---------------\n\nEnter your choice:\n");
}

void display(struct student *head)
{
	if(head==NULL)
		printf("No list is there in database\n");
	else
	{
		
			printf("--------------------------------------Student  Details--------------------------------------------\n");
		while(head)
		{
			printf("Student id             :%d\n",head->id);
			head=head->next;
		}
			printf("--------------------------------------------------------------------------------------------------\n");
	}
		
}

struct student * updateData(struct student *head,int position) {
                if(position<=0 || position > length) {
                        printf("cannot update the data at that position %d:\n",position);
                       return head;
                }
                int index;
              	 struct student *temp=head;
                for(index=1;index<position;index++) {
                        temp=temp->next;
                }
                printf("enter the id to be update name:\n");
                scanf("%d",&temp->id);
		return head;
              }


void search(struct student *head,int data)
{
	int flag=0;
	while(head!=NULL && head->id!=data )
	{
		head=head->next;
		flag++;
	}
	if(head==NULL) {

		printf("%d was not found in the list \n",data);
	}
	else {
	printf("---------------------------data found successfully--------------------------\n");
	printf("ROLLNO             :%d\n",head->id);
	}
}


struct student *addatBeginning(struct student *head)
{
	struct student *node=NULL;
	node=calloc(1,sizeof(struct student));
	if(node==NULL)
		printf("Error\n");
	printf("Enter your id:\n");
	scanf("%d",&node->id);
	node->next=head;
	head=node;
	return head;
}

struct student *addatEnd(struct student *head)
{
	struct student *node=NULL,*temp=NULL;
        node=calloc(1,sizeof(struct student));
        if(node==NULL)
                printf("Error\n");

        printf("Enter your id:\n");
        scanf("%d",&node->id);
	if(head==0)
		head=node;
	else
	{
		temp=head;
		while(temp->next)
			temp=temp->next;
		temp->next=node;
	}
	return head;
}

struct student *addatMiddle(struct student *head)
{
	struct student *node=NULL,*fast=NULL,*slow=NULL;
        node=calloc(1,sizeof(struct student));
        if(node==NULL)
                printf("Memory not allocated\n");

        printf("Enter your rollno:\n");
        scanf("%d",&node->id);
	if(head == NULL) {
         printf("list is empty so directly adding the record to list\n");
       head = node;
        return head;
    }
   
    if((head)->next == NULL) {
       int choice=0;
         printf("only one record is lresent in the list. want to add at 1.first or 2.last?\nenter your choice:");
         if(choice==1) {
           node->next = head;
             head = node;
             return head;
         }
         else if(choice==2) {
           (head)->next=node;
           return head;
         }
         else {
         printf("invalid choice. exiting...");
         return head; 
    }
    }
    else if((head)->next->next==NULL) {
     node->next=head->next;
     (head)->next=node;
     return head;
     }

     for(fast=head,slow=head;fast->next!=NULL && fast->next->next!=NULL;fast=fast->next->next,slow=slow->next);
     node->next=slow->next;
     slow->next=node;
     return head;
}struct student *deleteatBeginning(struct student *head)
{
	struct student *temp=NULL;
	if(head==0)
		printf("list is empty\n");
	else
	{
		temp=head;
		head=head->next;
		free(temp);
		temp=NULL;
	}
	return head;
}

struct student *deleteatPosition(struct student *head,int position)
{

		if(position <=0 || position > length) {
			printf("cannot delete the at %d position in list which is not there \n",position);
			return head;
		} 
		else if(position==1) {
			struct student *temp=head;
			head=head->next;
			free(temp);
			temp=NULL;
			length=length-1;
		} else {

			int index=1;
			struct student *currentnode=head;
			struct student  *previousnode=head;
			while(index !=position) {
				previousnode=currentnode;
				currentnode=currentnode->next;
				index=index+1;
			}
			previousnode->next=currentnode->next;
			free(currentnode);
			currentnode=NULL;
			length--;
		}
	

return head;
}



struct student  *deleteatEnd(struct student *head) {
     
      if(head==NULL) {
         printf("list is empty");
         return head;
      }
      struct student *temp,*previous=head;
      for(temp=head;temp->next!=NULL; previous=temp,temp=temp->next);
      previous->next=temp->next;
      free(temp);
      temp=NULL;
      return head;
}
