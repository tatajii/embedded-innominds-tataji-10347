    #include <stdio.h>
     
    int main()
    {
       int x = 7, y = 9, and, or, xor, right_shift, left_shift;
       
       and = x & y;
       or  = x | y;
       xor = x ^ y;
       left_shift = x << 1;
       right_shift = y >> 1;
       
       printf("%d AND %d = %d\n", x, y, and);
       printf("%d OR  %d = %d\n", x, y, or);
       printf("%d XOR %d = %d\n", x, y, xor);
       printf("Left shifting %d by 1 bit = %d\n", x, left_shift);
       printf("Right shifting %d by 1 bit = %d\n", y, right_shift);
       
       return 0;
    }
