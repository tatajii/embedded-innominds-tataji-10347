#include<stdio.h>

int main() {

	int number,binary=7,str[10],i=0;
	printf("enter number:");
	scanf("%d",&number);

	for(binary=7;binary>=0;binary--) {
		int temp;
		temp=number>>binary;
		if(temp&1) {
			str[i]=1;
			i++;
		}
		else {
			str[i]=0;
			i++;
		}
	}
	for(i=0;i<8;i++) {
		printf("%d ",str[i]);
	}
	printf("\nafter toggling even bits :\n");
	for(i=0;i<8;i=i+2) {
		if(str[i]==1) {
			str[i]=0;
		}
		else if(str[i]==0) {
			str[i]=1;
		}
	}
	for(i=0;i<8;i++) {
		printf("%d ",str[i]);
	}

}
