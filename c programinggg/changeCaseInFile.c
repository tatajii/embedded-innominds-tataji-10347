#include<stdio.h>
#include<ctype.h>

int main() {
	FILE *fp1,*fp2;
	char ch;
	fp1=fopen("abc.txt","r");
	fp2=fopen("def.txt","w");
	
	while((ch=fgetc(fp1))!=EOF) {
		ch=toupper(ch);
		fputc(ch,fp2);
	}
	fclose(fp1);
	fclose(fp2);
	return 0;
}

