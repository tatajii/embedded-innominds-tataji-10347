/****************************************************************************************************** 
	
  	Author		
        -------
	I.TATAJI	10347	tatajiirrinki@gmail.com	    Cell : 9704147867

        Purpose:
        --------
	  This program is to implement the queue,which follows First in First out approach.
         
       

******************************************************************************************************/
	
#include<iostream>
#include<stdio.h>
#include<stdlib.h>
using namespace std;
// creating class myqueue
class MyQueue {
	// declaring variables
	struct queue {
		string name;		
		struct queue *next;
	}*front,*rear;
   public:
	// constructor 
	MyQueue() {
		front=NULL;
		rear=NULL;
	}
	// method to add data into the queue
	void enque(string name) {
		struct queue *new_record=new struct queue;
		new_record->name=name;
		if(front==NULL) {
			front=new_record;
			rear=new_record;
			new_record->next=NULL;
			return;
		}
		rear->next=new_record;
		rear=new_record;
		return;
	}
	//method to delete the data from queue
	string deque() {
		string data;
		if(front==NULL) {
			cout<<"Queue is empty"<<endl;
			return NULL;
		}
		struct queue *temp=front;
		data=temp->name;
		front=front->next;
		delete temp;
		temp=NULL;
		return data;
	}
	
	// method to displaying the data present in the queue 
	void displayQueue() {
		struct queue *temp=front;
		cout<<"\t*****records in queue:*****"<<endl;
		for(;temp!=NULL;temp=temp->next) {
			cout<<"\t\t"<<temp->name<<endl;
		}
	}
	~MyQueue() {
			delete front, rear;
	}	

};

int main(void) {
	// creating object  for class myqueue 
	MyQueue myqueue;
	// declaring variables
	int choice;
	string name;
	while(1) {
		cout<<"*****MENU*****"<<endl;
		cout<<"1.enque\n2.deque\n3.display records present in queue\n4.quit"<<endl;
		cout<<"enter your choice:"<<endl;
		cin>>choice;
		switch(choice) {
			case 1:
				cout<<"enter name:"<<endl;
				cin>>name;
				// calling enque method for add data into queue
				myqueue.enque(name);
				break;
			case 2:
				// calling deque method for delete the data from queue
				cout<<"record with data:"<<myqueue.deque()<<" was deleted"<<endl;
				break;
			case 3:
				// calling display method for displaying the data in the queue
				myqueue.displayQueue();
				break;
			case 4:
				exit(0);
			default:
				cout<<"invalid choice"<<endl;
		}
	} 


}
/************************************************************************************************
OUTPUT:
.......
*****MENU*****
1.enque
2.deque
3.display records present in queue
4.quit
enter your choice:
1
enter name:
tataji
******************************************************************
*****MENU*****
1.enque
2.deque
3.display records present in queue
4.quit
enter your choice:
1
enter name:
avinash
*******************************************************************
*****MENU*****
1.enque
2.deque
3.display records present in queue
4.quit
enter your choice:
3
	*****records in queue:*****
		tataji
		avinash
*****************************************************************
*****MENU*****
1.enque
2.deque
3.display records present in queue
4.quit
enter your choice:
2
record with data:tataji was deleted
******************************************************************
*****MENU*****
1.enque
2.deque
3.display records present in queue
4.quit
enter your choice:
3
	*****records in queue:*****
		avinash
**********************************************************************************/
