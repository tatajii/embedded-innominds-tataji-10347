#include<stdio.h>
#include<stdlib.h>


// creating structure and named as node
struct node {
	//initialising variables
	int data;
	struct node *next;
};
int length=0;
//add elements in the list in ascending order
void add(struct node **head, int num) {
	// initialising variables
	struct node *newnode=NULL,*temp=*head;
	// allocate memory to newnode
	newnode=calloc(1,sizeof(struct node *));
	newnode->data=num;
	//if list is empty
	if(*head==NULL||(*head)->data > num) {
		newnode->next=*head;
		*head=newnode;
		length++;
	}
		// if list has some values	
		else {	
			//traverse the list
			while(temp!=NULL) {
				//checking the new data with existing data
				if(temp->data<=num&&(temp->next==NULL||temp->next->data>num)) {
					// if entered num is already exist
					if(temp->data==num) {
					return;
					}
					// otherwise
					else {	
						newnode->next=temp->next;
						temp->next=newnode;
						length++;
						return;
					}
				}
				temp=temp->next;
		}
		}
}
// printing the data in the list
void display(struct node *head) {
	if(head==NULL) {
		printf("list is empty");
		return;
	}
	else {
		while(head!=NULL) {
			printf("%d ",head->data);
			head=head->next;

		}
	}
	printf("\n");
}

// function for merge two linked list
void merge(struct node *f,struct node *s,struct node **head) {
	// initialising variables
	struct node *temp=*head;
	if(f==NULL&&s==NULL) {
		return;
	}
	// traversing the both list
	while(f!=NULL &s!=NULL) {
		// if list is empty
		if(*head==NULL) {
			//creating memory to temp variable and assign to head
			temp=calloc(1,sizeof(struct node *));
			*head=temp;
		}
		// list is not empty
		else {
			//creating memory to temp variable and assign 
			temp->next=calloc(1,sizeof(struct node *));
			temp=temp->next;
		
		}
		//if first list data is lessthan second list data
		if(f->data < s->data) {
			temp->data=f->data;
			f=f->next;
			length++;
		}
		else {
			//if second list data is lessthan first list data
			if(s->data < f->data) {
				temp->data=s->data;
				s=s->next;
			length++;
			}
			//if first list data and second list data is same
			else {
				if(f->data==s->data) {
					temp->data=f->data;
					f=f->next;
					s=s->next;
			length++;
				}
			}
		}
	}
	//traversing first list
	while(f!=NULL) {
		temp->next=calloc(1,sizeof(struct node *));
		temp=temp->next;
		temp->data=f->data;
		f=f->next;
			length++;
	}
	// traversing second list
	while(s!=NULL) {
		temp->next=calloc(1,sizeof(struct node *));
		temp=temp->next;
		temp->data=s->data;
		s=s->next;
			length++;
	}
	temp->next=NULL;
}




// main function
int main() {
	//initialisation
	struct node *first=NULL,*second=NULL,*third=NULL;
	// calling add function
	add(&first,2);
	add(&first,3);
	add(&first,1);
	add(&first,4);
	add(&first,2);
	//calling display function 
	printf("\ndisplay first list\n");
	display(first);
	printf("length of first list %d",length);
	length=0;
	// calling add function
	add(&second,5);
	add(&second,8);
	add(&second,9);
	add(&second,6);
	//calling display function 
	printf("\ndisplay second list\n");
	display(second);
	printf("length of second list %d",length);
	length=0;
	merge(first,second,&third);
	printf("\nafter merging list\n");
	//calling display function 
	display(third);
	printf("length  after merging two list %d",length);
	// calling add function
	add(&third,7);
	printf("\ndisplay third list\n");
	//calling display function 
	display(third);
	printf("length of third list %d",length);
	return 0;
}
	


