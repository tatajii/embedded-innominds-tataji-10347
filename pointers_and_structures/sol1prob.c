#include<stdio.h>

struct s{
	char ch;
	int i;
	float a;
};
void f(struct s v) {
	printf("\n%c %d %f",v.ch,v.i,v.a);
}
void g(struct s *v) {
	printf("\n%c %d %f",v->ch,v->i,v->a);
}
int main() {
	struct s var={'a',23,17.23};
	f(var);
	g(&var);
	return 0;
}

