#include<stdio.h>
#include<stdlib.h>
int length=0;
struct node{
	int data;
	struct node *next,*prev;
};

void addatBegin(struct node **head) {
	struct node *newnode=NULL;
	newnode=calloc(1,sizeof(struct node*));
	printf("enter data: ");
	scanf("%d",&newnode->data);
	if(*head==NULL) {
		*head=newnode;
		length++;
	}
	else{
		(*head)->prev=newnode;
		newnode->next=*head;
		*head=newnode;
		length++;
	}

}
void display(struct node *head) {
	if (head==NULL) {
		printf("list is empty");
	}
	else {
		while(head!=NULL){
		//	printf("%d ",head->data);
			printf("\n%d address=%p next=%p\t",head->data,head,head->next);
                        //printf("\n%d address=%p next=%p\t",head->data,head,head->next);

			head=head->next;
		}
	}
}
void addatEnd(struct node **head){
	struct node *temp=*head,*newnode=NULL;
	newnode=calloc(1,sizeof(struct node*));
	printf("enter data: ");
	scanf("%d",&newnode->data);
	if(*head==NULL) {
		*head=newnode;
		length++;
	}
	else {
		while(temp->next!=NULL) {
			temp=temp->next;
		}
		temp->next=newnode;
		newnode->prev=temp;
		newnode->next=NULL;
		length++;
	}
}




void addatPosition(struct node **head){
	struct node *next=NULL,*prev=NULL,*temp=*head,*newnode=NULL;
	int position;
	newnode=calloc(1,sizeof(struct node*));
	printf("enter position");
	scanf("%d",&position);
	while(position==0){
		printf("enter position from 1 to %d",length);
		printf("\nenter position\n");
		scanf("%d",&position);
	}

	if(position<=0 ||position>length) {
		printf("\n list is empty\n");
		return;
	}
	if(position==1) {
		addatBegin(head);
	}
	else {
		printf("\nenter data");
		scanf("%d",&newnode->data);
	       int index=1;
	       while(index!=(position-1)) {
		       temp=temp->next;
		       index++;
	       }
	       newnode->next=temp->next;
	       temp->next=newnode;
	       newnode->prev=temp;
	       length++;
	}

}



void deleteatPosition(struct node **head) {
	struct  node *temp=*head;
	int position;
	if(length==0) {
		printf("list is empty");
		exit(0);
	}
	printf("enter the position");
	scanf("%d",&position);
	while(position==0 ||position>length){
		printf("enter position from 1 to %d",length);
		printf("\nenter position\n");
		scanf("%d",&position);
	}
	if(position==1){
		if((*head)->next==NULL) {
			free(*head);
			*head=NULL;
			length--;
		}
		else{
			*head=(*head)->next;
			free(temp);
			temp=NULL;
			length--;
		}
	}
	else {
		int index=1;
		while(index!=(position-1)){
			temp=temp->next;
			index++;
		}
		struct node *temp1=temp->next;
		temp->next=temp->next->next;
		free(temp1);
		temp1=NULL;
		length--;
	}
			
	}
int main() {
        struct node *head=NULL;
        int choice,position,element;
        while(1) {
                printf("\nenter your choice for doublelinkedlist:");
                printf("\n1.addatBegin\n2.add atEnd\n3.addatPosition\n4.display\n5.delete at position\n6.exit\n");
                scanf("%d",&choice);
                switch(choice) {
                        case 1:addatBegin(&head);
                               break;
                        case 2:addatEnd(&head);
                                break;
                        case 3:addatPosition(&head);
                                break;
                        case 4: display(head);
                                break;
                        case 5: deleteatPosition(&head);
                                break;
			case 6:exit(0);
		}
	}
} 
