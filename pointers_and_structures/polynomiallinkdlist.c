#include<stdio.h>
#include<stdlib.h>

struct polynode {
	float coef;
	int expo;
	struct polynode *next;
};

void poly_add(struct polynode **head,float a,int b) {
	struct polynode *temp=*head,*newnode=NULL;
	newnode=calloc(1,sizeof(struct node*));
	if(*head==NULL) {
		*head=newnode;
		newnode->next=NULL;
	}
	else {
		while(temp->next!=NULL) {
			temp=temp->next;
		
		}
		temp->next=newnode;
		newnode->next=NULL;
	}
	newnode->coef=a;
	newnode->expo=b;
}
void display(struct polynode *head) {
	while(head!=NULL) {
		printf("%.1fx^%d: ",head->coef,head->expo);
		head=head->next;
	}
	printf("\b\b\b ");
}
       	


void poly_addition(struct polynode *f,struct polynode *s,struct polynode **head) {
	struct polynode *temp=*head;

	if(f==NULL&&s==NULL) 
		return;
	while(f!=NULL&&s!=NULL) {
		if(temp==NULL) {
			temp=calloc(1,sizeof(struct polynode *));
			*head=temp;
		}
		else {
			temp->next=calloc(1,sizeof(struct polynode *));
			temp=temp->next;
		
		}
	

		if(f->expo < s->expo) {
			temp->coef=s->coef;
			temp->expo=s->expo;
			s=s->next;
		}
		else {
			if(f->expo > s->expo) {
				temp->coef=f->coef;
				temp->expo=f->expo;
				f=f->next;
			}
			else{
			       if(f->expo==s->expo) {
				temp->expo=s->expo+f->expo;
				temp->coef=s->coef+f->coef;
				s=s->next;
				f=f->next;
			}
		}
	}
}
	while(f!=NULL) {
		if(*head==NULL) {
			*head=calloc(1,sizeof(struct polynode*));
			temp=*head;
		}
		else {
			temp=calloc(1,sizeof(struct polynode*));
			temp=temp->next;
		}
		temp->expo=f->expo;
		temp->coef=f->coef;
		f=f->next;
	}
	while(s!=NULL) {
		if(*head==NULL) {
			*head=calloc(1,sizeof(struct polynode*));
			temp=*head;
		}
		else {
			temp=calloc(1,sizeof(struct polynode*));
			temp=temp->next;
		}
		temp->expo=s->expo;
		temp->coef=s->coef;
		s=s->next;
	}
	temp->next=NULL;
	}






int main() {

	struct polynode *first=NULL,*second=NULL,*total=NULL;
	int i=0;

	poly_add(&first,1.7,3);
	poly_add(&first,2.3,2);
	poly_add(&first,4.6,1);
	poly_add(&first,23,0);
	display(first);
	poly_add(&second,2.3,3);
	poly_add(&second,1.7,2);
	poly_add(&second,2.3,1);
	poly_add(&second,17,0);
	printf("\n\n");
	display(second);
	printf("\n");
	while(++i<79) {
		printf("-");
	}
	printf("\n\n");
	poly_addition(first,second,&total);
	display(total);

	return 0;
}
