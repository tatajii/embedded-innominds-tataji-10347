/*****************************************************************************************
AUTHOR:
-------
	TATAJI.IRRINKULA   Emp_Id-10347   email: tatajiirrinki@gmail.com  9704147867
	
	Purpose:
	--------
		In this program, remove the duplicate characters in a string.


*******************************************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int main(){
	int i,j;
	char string[50]="silence is a source of great strength";
	printf("%s\n",string);
	for(i=0;string[i];i++){
		char temp=string[i];
		for(j=i+1;string[j];j++){
			if(string[j]==temp){
				memmove(string+j,string+j+1,strlen(string+j+1)+1);
				j--;

			}
		}
	}
	printf("after removing the duplicate characters in a string: %s\n",string);
}

/*******************************************************************************************
OUTPUT:
------
	silence is a source of great strength
	after removing the duplicate characters in a string: silenc aourfgth

*********************************************************************************************/
