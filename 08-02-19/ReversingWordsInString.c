/************************************************************************************************
AUTHOR:
-------
	TATAJI.IRRINKULA  Emp_Id-10347  email: tatajiirrinki@gmail.com  9704147867

	Purpose:
	--------
		this program is used to reverse the order of word's in a string given by user.

 * *********************************************************************************************/
#include<stdio.h>
#include<string.h>
void stringReverse(char string[]) {
	int i=0,j,count1=0,count2=strlen(string)-1;
	//char string2[40],temp[50];
	for(i=strlen(string)-1;i>=0;i--) {
		if(string[i]==' '|| i==0) {
			if(i==0) {
				count1=0;
			}
			else {
				count1=i+1;
			}

			for(j=count1;j<=count2;j++) {
				printf("%c",string[j]);
			}
			count2= i-1;
			printf(" ");
		}

	}
	printf("\n");
}
int main() {
	char string[80];
	printf("Enter the string:\n");
	gets(string);
	puts(string);
	printf(" After reverse the words in given string is:\n");
	stringReverse(string);
	return 0;
}
/************************************************************************************************
 OUTPUT:
--------
Enter the string:
this is innominds embedded batch
this is innominds embedded batch
 After reverse the words in given string is:
batch embedded innominds is this 

*************************************************************************************************/
