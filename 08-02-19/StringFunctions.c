/****************************************************************************************
 AUTHOR:
--------
	TATAJI.IRINKULA    Emp_Id-10347    email: tatajiirrinki@gmail.com   9704147867
 	
	Purpose:
	-------
		In this programm four operations on strings using string functions


****************************************************************************************/
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
strtoken() {
 char string[]="this is innominds embedded team";
	char* token=strtok(string," ");
	while(token!=NULL) {
	printf("%s\n",token);
	token=strtok(NULL," ");
	}
}
int main() {
int choice;
char string1[40]=" hello freinds";
char string2[40]=" good morning";
while(1) {
printf("\n**********************Menu************************\n");
printf("1.string concatination\n2.string token\n3.string in string\n4.string copy\n5.exit");
printf("\nenter your choice:");
scanf("%d",&choice);
switch(choice) {
case 4: strcpy(string2,string1);
	printf("%s",string2);
	break;
case 1: 

	strcat(string1,string2);
	printf("%s",string1);
	break;
case 2: strtoken();
	break;
case 3: printf("\n\n %s", strstr(string1,string2));
	break;
case 5: exit(0);

}
}
return 0;
}
/*************************************************************************************************
 OUTPUT:
--------
	**********************Menu************************
1.string concatination
2.string token
3.string in string
4.string copy/
5.exit
enter your choice:1
 hello freinds good morning
**********************Menu************************
1.string concatination
2.string token
3.string in string
4.string copy/
5.exit
enter your choice:2
this
is
innominds
embedded
team

**********************Menu************************
1.string concatination
2.string token
3.string in string
4.string copy
5.exit
enter your choice:3


 good morning
**********************Menu************************
1.string concatination
2.string token
3.string in string
4.string copy/
5.exit
enter your choice:4
 hello freinds good morning
**********************Menu************************
1.string concatination
2.string token
3.string in string
4.string copy/
5.exit
enter your choice:5



*******************************************************************************************/
