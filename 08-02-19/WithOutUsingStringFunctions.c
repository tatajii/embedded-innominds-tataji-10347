/****************************************************************************************
AUTHOR:
--------
	TATAJI.IRINKULA    Emp_Id-10347    email: tatajiirrinki@gmail.com   9704147867

	Purpose:
	-------
		In this programm four operations on strings done by using without string functions


 ****************************************************************************************/
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
void strtoken(char str[]) {
	int count =0,i;
	for(i=0;str[i]!='\0';i++) {
		count++;

	}
	for(i=0;i<count+1;i++) {
		str[i]!=' ' ? printf ("%c",str[i]) : printf("\n"); 
	}
}
void copyString(char string1[],char string2[]) {
	int c=0;
	while(string1[c]!='\0') {
		string2[c]=string1[c];
		c++;
	}
	string2[c]='\0';

}

void stringCat(char string1[],char string2[]) {
	int count1=0,count2=0;
	while(string1[count1]!='\0') {
		count1++;
	}
	while(string2[count2]!='\0') {
		string1[count1]=string2[count2];
		count2++;
		count1++;
	}
	string1[count1]='\0';
}
int stringStr(char string1[],char string2[]) {
	int count1=0,count2=1,i,j,flag=0;
	while(string2[count1]!='\0') {
		count1++;
	}

	for(i=0;string1[i];i++) {
		if(string1[i]==string2[i]) {
			for (j=i+1;string2[j]!='\0';j++) {
				if(string2[j]==string1[j]) {
					count2++;
				}
			}

		}
		if (count1==count2) {

			flag=1;
			return flag;
		}



	}

	if(flag==1) {

		printf(" string2 is present string1");
	}
	else {

		printf(" string2 is not present string1");
	}


}
int main() {
	int choice;
	char string1[40]=" hello freinds";
	char string2[40]=" good morning";
	while(1) {
		printf("\n**********************Menu************************\n");
		printf("1.string concatination\n2.string token\n3.string in string\n4.string copy\n5.exit");
		printf("\nenter your choice:");
		scanf("%d",&choice);
		switch(choice) {
			case 4: copyString(string1,string2);
				printf("the data present in string2 is: %s \n",string2);
				break;
			case 1: 

				stringCat(string1,string2);
				printf("the concatination of two strings is: \n %s", string1);
				break;
			case 2:  strtoken(string1);;
				 break;
			case 3:  		
				 stringStr(string1,string2);
				 break;
			case 5: exit(0);

		}
	}
	return 0;
}
/*************************************************************************************************
OUTPUT:
--------
**********************Menu************************
1.string concatination
2.string token
3.string in string
4.string copy
5.exit
enter your choice:1
the concatination of two strings is: 
  hello freinds good morning
**********************Menu************************
1.string concatination
2.string token
3.string in string
4.string copy
5.exit
enter your choice:2

hello
freinds
good
morning
**********************Menu************************
1.string concatination
2.string token
3.string in string
4.string copy
5.exit
enter your choice:3
 string2 is not present string1
**********************Menu************************
1.string concatination
2.string token
3.string in string
4.string copy
5.exit
enter your choice:4
the data present in string2 is:  hello freinds good morning 

**********************Menu************************
1.string concatination
2.string token
3.string in string
4.string copy
5.exit
enter your choice: 5
  *******************************************************************************************/
