/***************************************************************************************
AUTHOR:
-------

	TATAJI.IRRINKULA   Emp_Id-10347   email:tatajiirrinki@gmail.com    9704147867

	Purpose:
	--------
		In this program, create a student data and store that into a csv file and 
	display student details.  

*******************************************************************************************/
#include <stdio.h>
#include <stdlib.h>

typedef struct student {
	int id;
	char name[20];
}stu;

int main(void) {

	FILE *fp=NULL;
	int ch;
	int count=3;
	stu *temp=(stu*)malloc(sizeof(stu)*count);
	if(temp == NULL) {
		printf("Failed to memory allocation \n");
		exit(0);
	}
	//opening the file to write
	if((fp = fopen("csv.csv","a+")) == NULL) {
		printf("Failed to open the file\n");
		exit(0);
	}
	else
		printf("FILE OPENED SUCCESSFULLY\n");
	while(count!=0) {
		printf("Enter  student id :\n");
		scanf("%d",&temp->id);
		getchar();
		fprintf(fp,"%d",temp->id);
		printf("Enter student  name :\n");
		fgets(temp->name,20,stdin);
		fputs(".",fp);
		fputs(temp->name,fp);
		
		fputs("\n",fp);

		
		count--;
	}
	free(temp);
	//closing the file
	fclose(fp);


	//opening the file to read the contents
	if((fp = fopen("csv.csv","r")) == NULL) {
		printf("Failed to open file\n");
		exit(0);
	}
	else
		printf("OPENED SUCCESSFULLY\n");
	printf("printing the conents of csv file\n");
	printf("--------------------------------------\n");
	while((ch=fgetc(fp))!=EOF){
		printf("%c",ch);
	}

	printf("------------------------------------------\n");
	//closing the csv file
	fclose(fp);
	return 0;
}
/*****************************************************************
OUTPUT:
-------
FILE OPENED SUCCESSFULLY
Enter  student id :
1
Enter student  name :
tataji
Enter  student id :
2
Enter student  name :
saii
Enter  student id :
3
Enter student  name :
mani
OPENED SUCCESSFULLY
printing the conents of csv file
--------------------------------------
1.tataji

2.saii

3.mani

------------------------------------------
**************************************************************/
