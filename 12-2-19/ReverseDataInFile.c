/********************************************************************************
 AUTHOR:
 -------
	TATAJI.IRRINKULA  Emp_Id:10347  email:tatajiirrinki@gmail.com  9704147867

	Purpose:
	-------
		In this program,the data in file1 can be changed to reverse order 
	and store in file2.

	 
********************************************************************************/

#include <stdio.h>
#include <stdlib.h>

int main(void) {

	FILE *fp1=NULL,*fp2=NULL;
	char ch;int len;
	if((fp1 = fopen("file1.txt","r")) == NULL) {
		perror("fp1");
		return -1;
	}
	else 
		printf("FILE -> 1 OPENED SUCCESSFULLY\n");
	if((fp2 = fopen("file2.txt","w")) == NULL) {
		perror("fp2");
		return -1;
	}
	else 
		printf("FILE -> 2 OPENED SUCCESSFULLY\n");
	while((ch = fgetc(fp1)) != EOF){
		printf("%c",ch);
	}
	fseek(fp1,-1,SEEK_END);
	len = ftell(fp1);
	printf("%d",len);
	while(len >= 0){
		ch = fgetc(fp1);
		fputc(ch,fp2);
		fseek(fp1,-2,SEEK_CUR);
		len--;
	}	
	printf("\n");
	fclose(fp1);
	fclose(fp2);
	return 0;
}
/********************************************************************************
output:
------
FILE -> 1 OPENED SUCCESSFULLY
FILE -> 2 OPENED SUCCESSFULLY
 hi innominds

14
[fedora@localhost cprograms]$ cat file2
cat: file2: No such file or directory
[fedora@localhost cprograms]$ cat file2.txt 


sdnimonni ih  
********************************************************************************/

