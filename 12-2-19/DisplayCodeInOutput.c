/****************************************************************************************
 AUTHOR:
 -------
	TATAJI.IRRINKULA  Emp_Id-10347  email:tatajiirrinki@gmail.com 9704147867

	Purpose:
	-------
		In this program, the content in .c file can be displayed in command
	prompt. 

*****************************************************************************************/

#include <stdio.h>
#include <stdlib.h>

int main(void) {

	FILE *fp = NULL;
	fp = fopen(__FILE__,"r");	
	//This give the file name which is we were given.

	if(fp == NULL) {
		perror("fp");
		return -1;
	}
	printf("FILE OPENED SUCCESSFULLY\n");
	int ch;
	while((ch=fgetc(fp)) != -1) {
		printf("%c",ch);
	}
	fclose(fp);
	return 0;
}
/****************************************************************************************
OUTPUT:
-------
	FILE OPENED SUCCESSFULLY
#include <stdio.h>
#include <stdlib.h>

int main(void) {

	FILE *fp = NULL;
	fp = fopen(__FILE__,"r");	
	//This give the file name which is we were given.

	if(fp == NULL) {
		perror("fp");
		return -1;
	}
	printf("FILE OPENED SUCCESSFULLY\n");
	int ch;
	while((ch=fgetc(fp)) != -1) {
		printf("%c",ch);
	}
	fclose(fp);
	return 0;
}
 
*****************************************************************************************/
