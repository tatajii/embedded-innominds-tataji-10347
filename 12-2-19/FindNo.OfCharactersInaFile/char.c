/*************************************************************************************
 AUTHOR:
--------
	TATAJI.IRRINKULA  Emp_Id-10347  email:tatajiirrinki@gmail.com 9704147867
	
	Purpose:
	-------
		In this program,calulate the no.of words in atext file,no.of lines,no.of
	characters and total size of that text file can be find.	 


***************************************************************************************/
#include <stdio.h>
#include <stdlib.h>

int main(void) {

	FILE *fp=NULL;
	if((fp=fopen("file1.txt","r")) == NULL) {
		printf("Filed to open file\n");
		return -1;
	}
	printf("FILE OPEND SUCCESSFULLY\n");
	char ch;
	int linecount=0,wordcount=0,charactercount=0;
	while((ch=fgetc(fp))!=-1) {
		if(ch == ' ')
			wordcount++;
		else if(ch == '\n')
			linecount++;
		else
			charactercount++;
	}
	fseek(fp,0,SEEK_END);
	printf("Total Number of words :%d\n",wordcount+linecount);
	printf("Total Number of Lines :%d\n",linecount);
	printf("Total Number of charecters :%d\n",charactercount);
	printf("The size of File : %ld\n",ftell(fp));
	fclose(fp);
	return 0;
}
/***********************************************************************
 OUTPUT:
--------

FILE OPEND SUCCESSFULLY
Total Number of words :6
Total Number of Lines :2
Total Number of charecters :27
The size of File : 33
[fedora@localhost Downloads]$ cat file1.txt 
hi every one
helllo good morning
************************************************************************/





