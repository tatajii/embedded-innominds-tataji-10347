/*******************************************************************************
 AUTHOR:
 -------
	TATAJI.IRRINKULA  Emp_Id:10347  email:tatajiirrinki  9704147867

	Purpose:
	--------
		In this program, the data present in two files will merge and
	 save data into another file. 
********************************************************************************/
#include<stdio.h>
#include<stdlib.h>

int main() {
	int a=1,b=1;
	char data[100];
	char ch, file1[20],file2[20],file3[20];
	FILE *fp1,*fp2,*ftemp;
	printf("enter the first file name:\n");
	gets(file1);
	fp1=fopen(file1,"w");
			
 	while(b!=0) {
		printf("enter the data:\n");
		gets(data);
		fprintf(fp1,"%s",data);
		b--;
		}
	fclose(fp1);
	printf("enter the second file name:\n");
	gets(file2);
	fp2=fopen(file2,"w");

	while(a!=0) {
		printf("enter the data:\n");
		gets(data);
		fprintf(fp2,"%s",data);
		a--;
		}
	
	fclose(fp2);
	printf("enter the third file name:\n");
	gets(file3);
	ftemp=fopen(file3,"w+");
	
	fp1=fopen(file1,"r");
	fp2=fopen(file2,"r");
	if(fp1==NULL||fp2==NULL) {
		printf("file is not opened");
	}

	if (ftemp==NULL) {

		printf("file is not opened");
}



	while(1) {
		ch=fgetc(fp1);
		if(ch==EOF)
			break;	
		fputc(ch,ftemp);
	}
	
	while(1) {
		ch=fgetc(fp2);
		if(ch==EOF)
			break;	
		fputc(ch,ftemp);
	}
	printf("merge files successfully");
	
	ftemp=fopen(file3,"w+");
	fclose(fp1);
	fclose(fp2);

	fclose(ftemp);

	return 0;

}
/******************************************************************************
 OUTPUT:
--------
	enter the first file name:
a.txt
enter the data:
hi
enter the second file name:
b.txt
enter the data:
 hello
enter the third file name:
c.txt
merge files successfully[fedora@localhost tataji]$ cat c.txt 
hi hello

*******************************************************************************/
