/***********************************************************************************************
AUTHOR:
-------
	TATAJI.IRRINKULA       Emp_Id-10347   eamil:tatajiirrinki@gmail.com   9704147867

	Purpose:
	-------
		In this program, reads data from a CSV file,make the data into 2-d array
	 matrix format and dispal the data.
 ******************************************************************************************/
#include<stdio.h>
int main() {
	FILE *fp1=NULL;
	int ch,count=0;
	if((fp1=fopen("matrix.csv","r")) == NULL) {
		printf("Failed to open the file.........\n");
		return -1;
	}
	else
		printf("File1 has  Opened succesfully\n");

	while((ch = fgetc(fp1)) != -1 ) {
		if(ch==',' || ch==10){
		}else {
			count+=1;
		}
	}
	printf("the elements in the CSV file are : %d\n",count);
	rewind(fp1);
	int rows,columns;
	int i=0,j=0;
	printf("enter the rows of a matrix\n");
	scanf("%d",&rows);
	printf("enter the columns of a matrix\n");
	scanf("%d",&columns);
	while(rows*columns<count) {
		printf("the entered rows and columns product %d is less than total numners %d \n",rows*columns,count);
		printf("enter the rows of a matrix\n");
		scanf("%d",&rows);
		printf("enter the columns of a matrix\n");
		scanf("%d",&columns);
	}


	int matrix[rows][columns];

	while((ch = fgetc(fp1)) != -1) {
		if(ch==',' || ch==10){

			j+=1;
			if(j>=columns) {
				j=0;
				i+=1;
			}

		}else {
			matrix[i][j]=ch-48;
		}

	}
	//making remianing elemnts in the matrix 0,if rows*columns>count
	for(i;i<=rows;i++) {
		for(j;j<=columns;j++) {
			matrix[i][j]=0;
		}
		j=0;
	}
	//printing matrix elements
	printf(" the elemenets in the matrix wriiten from CSV file are :\n");

	for(i=0;i<rows;i++) {
		for(j=0;j<columns;j++) {
			printf("%d ",matrix[i][j]);
		}
		printf("\n");
	}




	return 0;
}
/***********************************************************************************
OUTPUT:
-------
File1 has  Opened succesfully
the elements in the CSV file are : 20
enter the rows of a matrix
5
enter the columns of a matrix
4
 the elemenets in the matrix wriiten from CSV file are :
1 2 3 4 
5 0 9 8 
9 0 7 6 
8 4 5 1 
4 6 9 0 

***************************************************************************************/
