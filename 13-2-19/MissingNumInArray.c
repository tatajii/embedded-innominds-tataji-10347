/****************************************************************************************
AUTHOR:
------
	TATAJI.IRRINKULA  Emp_Id-10347  email:tatajiirrinki@gmail.com  9704147867
	Purpose:
	--------
             In this program, Finding the missing number in asorted Array and unsorted 
	array. 
	note: numbers in array should be taken from 0 to 10
*****************************************************************************************/

#include <stdio.h>
#define TOTAL 55
int main(void) {
	// finding the missing number in sorted array
	int arr[10] = {1,2,3,4,5,6,8,9,10};
	int i=0;
	while(i<10) {
		if((arr[i]+1) != (arr[i+1])) {
		      printf("Missing Number in Sorted Array :%d\n",arr[i]+1);
		      break;
		}
		i++;
	}
	// finding the missing number in un sorted array
	 int array[10]={10,9,5,8,1,6,2,3,4};
 	 int sum=0;
	 for(i=0;array[i];i++) {
		sum = sum + array[i];
	 }
	 if(TOTAL == sum) {
		 printf("There is no missing number in array.\n");
	 }
	 else 
		 printf("Missing Number in unsorted Array :%d\n",TOTAL-sum);
			 
	return 0;
}
/*****************************************************************************************
 OUTPUT:
-------

Missing Number in Sorted Array :7
Missing Number in unsorted Array :7

****************************************************************************************/
