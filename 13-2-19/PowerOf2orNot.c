/**************************************************************************************
 AUTHOR:
-------
	TATAJI.IRRINKULA  Emp_Id-10347  email:tatajiirrinki@gmail.com  9704147867

	Purpose:
	-------
		In this program, convert positive integer to 32bit format and check
	given number is power of 2 or not.
 * ************************************************************************************/

#include<stdio.h>
int main() {

	long number,binary,temp;
	int str[32],i=0,count=0;
	printf("enter number:\n");
	scanf("%d",&number)
	printf("the 32 bit binary format of number %d is:\n",number);

	for(binary=31;binary>=0;binary--) {
		temp=number >> binary;
		if(temp & 1) {
			str[i]=1;
			count++;
			i++;
		}
		else  {
			str[i]=0;
			i++;
		}
	}
	for(i=0;i<32;i++) {
		if(i==8 || i==16 ||i==24){
			printf(" ");
		}
		printf("%d",str[i]);
	}
	if(str[31]==1 || count > 1) {
		printf("\ngiven number %d is not a power of 2",number);
	}
	return 0;
}
/***************************************************************************************
OUTPUT:
------
enter number:
17
the 32 bit binary format of number 17 is:
00000000 00000000 00000000 00010001
given number 17 is not a power of 2
***************************************************************************************/
