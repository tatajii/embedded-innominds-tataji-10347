/**********************************************************************************************
AUTHOR:
-------
	TATAJI.IRRINKULA   Emp_Id-10347   email:tatajiirrinki@gmail.com     9704147867

	Purpose :
	--------
		In this program,performs postfix evaluation by using stack 

***********************************************************************************************/
#include<stdio.h>
#include<string.h>

int stack[20],n=20,top=-1;
//this function is used to store the data into stack
void push(int data)
{
	if(top>=n-1)
	{
		printf("\n\tSTACK is over flow");

	}
	else
	{

		top++;
		stack[top]=data;
	}
}
//this function is used to delete the data from stack
int pop()
{
	int data;
	if(top<=-1)
	{
		printf("\n\t Stack is under flow \n");
		return 0;
	}
	else
	{
		data= stack[top];
		top--;
	}
	return data;
}
int main()
{
	char str[20];
	int result=0,temp=0,number,i;
	printf("enter the postfix expression\n");
	fgets(str,20,stdin);
	for(i=0;i<strlen(str)-1;i++) {

		if(str[i]>=48 && str[i]<=57) {
			number=str[i]-48;
			push(number);
		} else if(str[i]=='(') {
			i++;
			number=0;
			while(str[i]!=')')
			{
				temp=str[i]-48;
				number=number*10+temp;
				i++;
			}
			push(number);
		} else {
			int second=pop();
			int first=pop();
			if(str[i]=='*') {

				result=first*second;
				push(result);
			}else if(str[i]=='/') {
				result=first/second;
				push(result);
			}else if(str[i]=='+') {
				result=first+second;
				push(result);
			}else if(str[i]=='-') {
				result=first-second;
				push(result);
			}
		}
	}
	printf("after the postfix expression evaluation the result is   :%d\n",result);


	return 0;

}
/****************************************************************************************
OUTPUT:
-------
enter the postfix expression
23(15)5/*+62*-4-
after the postfix expression evaluation the result is   :-5


****************************************************************************************/
