/****************************************************************************************
AUTHOR:
-------
	TATAJI.IRRINKULA   Emp_Id-10347   email:tatajiirrinki@gmail.com     9704147867

	Purpose :
	--------
 		This program perfoms swapping of two numbers without 	using temporary variable.

******************************************************************************************/
#include<stdio.h>
int main() {
int x=10,y=20;
printf("\n the values before swaping x=%d and y=%d",x,y);
x=x^y;
y=x^y;
x=x^y;
printf("\n the values after swaping x=%d and y=%d",x,y);
}
/****************************************************************************************
OUTPUT:
------
the values before swaping x=10 and y=20
 the values after swaping x=20 and y=10 

******************************************************************************************/


