/****************************************************************************************************** 

  Author          
  -------
  I.TATAJI        10347   tatajiirrinki@gmail.com     Cell : 9704147867

Purpose:
--------
	To sort the given array elents by using Insertion Sort technique.

 *********************************************************************************************************/

#include<iostream>

using namespace std;
//creating the InsertionSort class
class InsertionSort {
	//declaring the data members
	int *myarray;
	int size;
	//defining the member functions as public
    public:
	//parametrized constructor for InsertionSort class
	InsertionSort(int size) {
		this->size=size;
		myarray= new int[size];
		cout<<"enter array elements:"<<endl;
		for(int i=0;i<size;i++)
			cin>>myarray[i];
	}
	//member for insertionSort to sort the array elements
	void insertionSort() {
		int i,j,temp;
		for(int i=1;i<size;i++)
		{
			int j=i-1;
			temp=myarray[i];
			while(j>=0 && myarray[j]>temp) {
				myarray[j+1]=myarray[j];
				j--;
			}
			myarray[j+1]=temp;
		}

	}
	//method for printing the array elements 
	void printArray() {
		for(int i=0;i<size;i++)
			cout<<myarray[i]<<" ";
		cout<<endl;
	}
};

int main()
{
	// creating object for InsertionSort class
	InsertionSort object(5);
	cout<<"array elements before sorting:"<<endl;
	// calling print array method
	object.printArray();
	// calling insertionSort method
	object.insertionSort();
	cout<<"array elements after sorting:"<<endl;
	// calling print array method
	object.printArray();
	return 0;
}
/*****************************************************
OUTPUT:
-------
enter array elements:
8
2
5
7
1
array elements before sorting:
8 2 5 7 1 
array elements after sorting:
1 2 5 7 8 
 *******************************************************/
