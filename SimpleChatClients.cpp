/****************************************************************************************************** 
	
  	Author			
        -------
	I.TATAJI	10347	tatajiirrinki@gmail.com	Cell : 9704147867

        Purpose:
        --------
	This program is to communicate with the server present in anothe file and 
	reading the server's data by using shared memory segment.
		
	Here we are maintaing three clients by using threads. 

*******************************************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <iostream> 
#include <sys/ipc.h> 
#include <sys/shm.h> 
#include <unistd.h>

using namespace std;

// declaaring the global vvariables
static sem_t sem;
static int shmid;
static char *name;
char *str;

// creating client class
class Client {

	// declaring thread variables
	pthread_t thread1, thread2,thread3;

   public :

	// default clientt constructor
	Client() {
		
		//generating the unique key by using ftok
		key_t key = ftok("myfile1",85);
		//getting shared memory segment using shmget
		shmid = shmget(key,1024,0666|IPC_CREAT); 
		//attaching the shared memory to user string
		str = (char*) shmat(shmid,(void*)0,0); 
	}

	//method to read server data
	void readServerData() {
		
		//displaying the server's data
		cout<<str<<endl;
		//ditaching the shared memory segment from user string
		shmdt(str);
	}
		
	// method for getting client name 
	static void *clientName(void *arg) {
		
		//applying the lock by using sem_wait
		sem_wait(&sem);
		//reading the client name
		cout<<"enter client name :"<<endl;
		//attaching the shared memory to user string called name
		name=(char*) shmat(shmid,(void*)0,0); 
		cin>>name;
		sleep(10);
		//releasing the shared memory from user string
		sem_post(&sem);

	}

	// method to create threads 
	void createThreads() {

		pthread_create( &thread1, NULL,clientName, NULL);
		pthread_create( &thread2, NULL,clientName, NULL);
		pthread_create( &thread3, NULL,clientName, NULL);
	}

	//method to join threads
	void joinThreads() {

		pthread_join( thread1, NULL);
                pthread_join( thread2, NULL);
                pthread_join( thread3, NULL);

	}
};

int main()
{
	// creating object for client class
	Client c;
	// initialising semaphore
	sem_init(&sem, 0, 1);
	// calling readServerData method to read the server's data
	c.readServerData();
	//calling the createThreads method to create the threads
	c.createThreads();
	//calling the joinThreads method to join the threads
	c.joinThreads();
	// destroying semaphore
	sem_destroy(&sem);
	return 0; 

}
