/****************************************************************************************************** 
	
  	Author		
        -------
	I.TATAJI	10347	tatajiirrinki@gmail.com	    Cell : 9704147867

        Purpose:
        --------
		This program  is to  implement stack,which follows Last in First out approach.
        
       

******************************************************************************************************/
	
#include<iostream>
#include<stdio.h>
#include<stdlib.h>
using namespace std;
// creating class mystack
class MyStack {
	// declaring variables
	struct stack {
		string name;		
		struct stack *next;
	}*top;
   public:
	// constructor
	MyStack() {
		top=NULL;
	}
	// method for push the data into stack
	void push(string name) {
		struct stack *new_record=new struct stack;
		new_record->name=name;
		if(top==NULL) {
			top=new_record;
			new_record->next=NULL;
			return;
		}
		//new_record->name=name;
		new_record->next=top;
		top=new_record;
		return;
	}
	
	// method for pop the data from stack	
	string pop() {
		string data;
		if(top==NULL) {
			cout<<"stack underflow"<<endl;
			return NULL;
		}
		struct stack *temp=top;
		data=temp->name;
		top=top->next;
		delete temp;
		temp=NULL;
		return data;
	}
	
	// method for display the data present in stack
	void displayRecords() {
		struct stack *temp=top;
		cout<<"\t*****records in stack:*****"<<endl;
		for(;temp!=NULL;temp=temp->next) {
			cout<<"\t\t"<<temp->name<<endl;
		}
	}
	// destructor for mystack 
	  ~MyStack() {
                delete top;
        }

};

int main(void) {
	// creating object  for mystack class
	MyStack mystack;
	int choice;
	string name;
	while(1) {
		cout<<"*****MENU*****"<<endl;
		cout<<"1.push\n2.pop\n3.display records\n4.quit"<<endl;
		cout<<"enter your choice:"<<endl;
		cin>>choice;
		switch(choice) {
			case 1:
				cout<<"enter name:"<<endl;
				cin>>name;
				// calling push method
				mystack.push(name);
				break;
			case 2:
				// calling pop method
				cout<<"record with data:"<<mystack.pop()<<" was deleted"<<endl;
				break;
			case 3:
				// calling display method
				mystack.displayRecords();
				break;
			case 4:
				exit(0);
			default:
				cout<<"invalid choice"<<endl;
		}
	} 

 

}
/*****************************************************************************
OUTPUT:
........

*****MENU*****
1.push
2.pop
3.display records
4.quit
enter your choice:
1
enter name:
tataji
*************************************************************
*****MENU*****
1.push
2.pop
3.display records
4.quit
enter your choice:
1
enter name:
naidu
**************************************************************
*****MENU*****
1.push
2.pop
3.display records
4.quit
enter your choice:
3
	*****records in stack:*****
		naidu
		tataji
**************************************************************
*****MENU*****
1.push
2.pop
3.display records
4.quit
enter your choice:
2
record with data:naidu was deleted
***************************************************************
*****MENU*****
1.push
2.pop
3.display records
4.quit
enter your choice:
3
	*****records in stack:*****
		tataji
**********************************************************************************/




