/****************************************************************************************************** 
	
  	Author			
        -------
	I.TATAJI	10347	tatajiirrinki@gmail.com	Cell : 9704147867

        Purpose:
        --------
	This program is to communicate with the clients present in anothe file and 
	reading the clients data by using shared memory segment.

*******************************************************************************************************/

#include <iostream> 
#include <sys/ipc.h> 
#include <sys/shm.h>
#include <unistd.h>  
#include <stdio.h> 

using namespace std; 

// creating server class to using shared memory segment
class Server {
	
	int shmid ;
	char *str;
	public :

	// default constructor for server class which will get the shared memory
	Server() {

		key_t key = ftok("myfile1",85);
		shmid= shmget(key,1024,0666|IPC_CREAT); 
		str = (char *)shmat(shmid,(void*)0,0);  
		gets(str);
	}

	// method to print the client message 
	void writeClientData() {

		str = (char *)shmat(shmid,(void*)0,0);  
		cout<<"HELLO "<< str<<endl;
		shmdt(str);
	}
};




int main() 
{ 

	int i=0;

	// creating server object
	Server s;

	//iterating the loop for 3 times for 3 clients
	while(i<3) {
		i++;
		sleep(15);
		//calling the writeClientData method 
		s.writeClientData();
	}

	return 0; 
} 
