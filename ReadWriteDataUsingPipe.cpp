/*********************************************************************
	Author:
	-------
	I.TATAJI		Emp Id:10347	Cell No:9704147867
	

	Purpose:
	--------
	Create a pipe to comminicate two processes unidirectionally.

*********************************************************************/



#include<iostream>
#include<cstdlib>
#include<unistd.h>
#include<string.h>
#include<sys/types.h>
#include<sys/wait.h>

using namespace std;

int main(void) {
	
	//creat descriptors for pipe
	int file_id1[2];  
	int file_id2[2];
	char name[20];
	cout<<"Enter  the Name :"<<endl;
	cin>>name;
	
	//finding the length of string 
	int name_size = strlen(name)+1;
	pid_t file;
	
	if(pipe(file_id1)==-1) {
		cout<<"Failed to open the pipe: -1 "<<endl;
		return 0;	
	}
	if(pipe(file_id2)==-1) {
		cout<<"Failed to open the pipe: -2 "<<endl;
		return 0;	
	}

	file = fork();	//creating a child process
	
	if(file < 0) {
		cout<<"Fork operation is Failed "<<endl;
		return 0;
	} 

	//parent process 
	else if( file > 0) {
		char changed_str[50];
		close(file_id1[0]);   //closing the read end of pipe
		write(file_id1[1],name,name_size);
		close(file_id1[1]);   //closing the write end 
		
		//waiting upto child gives the status 
		wait(NULL);

		close(file_id2[1]);
		
		//read the data which is sent by the child process
		read(file_id2[0],changed_str,50);
		cout<<" The Data is : "<<endl;
		cout<<"\""<<changed_str<<"\""<<endl;
		close(file_id2[0]);
	}
	else {
		close(file_id1[1]);	//closing the write end
		char changed_name[50];
		read(file_id1[0],changed_name,50);
		char add_name[20];
		cout<<"Enter the Message you wants to add with Name : "<<endl;
		cin>>add_name;
		strcat(changed_name," ");
		strcat(changed_name,add_name);
		int result_size = strlen(changed_name)+1;
		
		//close the both read ends
		close(file_id1[0]);
		close(file_id2[0]);
		
		//write the data to the parent
		write(file_id2[1],changed_name,result_size);
		close(file_id2[1]);
	}

	return 0;		
}

/*******************************************************
OUTPUT:
------------
Enter  the Name :
tataji
Enter the Message you wants to add with Name : 
irrinkula
 The Data is : 
"tataji irrinkula"


*******************************************************/
