/***************************************************************************************
 AUTHOR:
 ------
	TATAJI.IRRINKULA  Emp_Id-10347  email: tatajiirrinki@gamail.com  9704147867

	Purpose:
	-------
		This program is used to see the status of a file.for ex: we have have to 
	see the last changes and size and permissions of that file.  


*****************************************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<unistd.h>
#include<time.h>


int main(int argc ,char **argv){
	struct stat v;
	//time_t;
//	struct tm *tmptr;
	//char datastring
	if(stat(argv[1],&v)==-1){
		perror("stat");
		return 0;
	}
	printf("size=%d\n",(int)v.st_size);
	printf("link count=%d\n",(int)v.st_nlink);
	printf("inode Number=%d\n",(int)v.st_ino);
	printf("FILE PERMISSIONS:\n");
	if((v.st_mode>>8)&1)
	printf("r");
	else 
		printf("-");

	if((v.st_mode>>7)&1)
	printf("w");
	else 
		printf("-");
	if((v.st_mode>>6)&1)
	printf("x");
	else 
		printf("-");
	if((v.st_mode>>5)&1)
	printf("r");
	else 
		printf("-");
	if((v.st_mode>>4)&1)
	printf("w");
	else 
		printf("-");
	if((v.st_mode>>3)&1)
	printf("x");
	else 
		printf("-");

	if((v.st_mode>>2)&1)
	printf("r");
	else 
		printf("-");
	if((v.st_mode>>1)&1)
	printf("w");
	else 
		printf("-");
	if((v.st_mode>>0)&1)
	printf("x");
	else 
		printf("-");
	printf("\n");
	if((v.st_mode>>15)&1){
		printf("regular file\n");
	}
	else if(((v.st_mode>>15)&1)&&((v.st_mode>>14)&1)){
		printf("socket file\n");
	}
	else if(((v.st_mode>>15)&1)&&((v.st_mode>>13)&1)){
		printf("linkfile\n");
	}
	else{
		printf("character file\n");
	}
	printf("uid:%u\n",v.st_uid);
	printf("gid:%u\n",v.st_gid);
	printf("mode:%d\n",v.st_mode);
	printf("blocks:%ld\n",v.st_blocks);
	printf("IO-blocks:%ld\n",v.st_blksize);
//	printf("%lu\n",v.st_rdev);
	
	printf("_--------------_-_-_____------------\n");

	printf("Access time = %s\n",ctime(&v.st_atime));
	printf("Modification time = %s\n",ctime(&v.st_mtime));
	printf("change time = %s\n",ctime(&v.st_mtime));
		
			
	return 0;
		
			
		
}
/*****************************************************************************************
 OUTPUT:
--------
[fedora@localhost Downloads]$ vi StatFileProperties.c 
[fedora@localhost Downloads]$ gcc StatFileProperties.c 
[fedora@localhost Downloads]$ ./a.out Ttt.txt 
size=56
link count=1
inode Number=268145
FILE PERMISSIONS:
rw-rw-r--
regular file
uid:1000
gid:1000
mode:33204
blocks:8
IO-blocks:4096
_--------------_-_-_____------------
Access time = Tue Feb 19 11:37:58 2019

Modification time = Tue Feb 19 11:37:58 2019

change time = Tue Feb 19 11:37:58 2019

[fedora@localhost Downloads]$ stat Ttt.txt 
  File: `Ttt.txt'
  Size: 56        	Blocks: 8          IO Block: 4096   regular file
Device: fd01h/64769d	Inode: 268145      Links: 1
Access: (0664/-rw-rw-r--)  Uid: ( 1000/  fedora)   Gid: ( 1000/  fedora)
Context: unconfined_u:object_r:user_home_t:s0
Access: 2019-02-19 11:37:58.951852223 +0100
Modify: 2019-02-19 11:37:58.951852223 +0100
Change: 2019-02-19 11:37:58.965852225 +0100
 Birth: -

******************************************************************************************/

