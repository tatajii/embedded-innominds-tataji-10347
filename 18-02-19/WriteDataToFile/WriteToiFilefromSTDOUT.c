/***************************************************************************************
AUTHOR:
--------
        TATAJI.IRRINKULA  Emp_ID-10347  email: tatajiirrinki@gmail.com  9704147867

        Purpose:
        -------
                This program is used to write data from command line to file  using 
                system calls.


*****************************************************************************************/
#include<stdio.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<unistd.h>
#include<string.h>

int main() {

	int fp1;
	char buff[100];
	fp1=open("Ttt.txt",O_WRONLY | O_CREAT);
	if (fp1 == -1)

		printf("Error in opening the file  \n"); 
	else 
			printf("input file opened in read mode\n");
	printf("write the conent that should be written into file \n");
	fgets(buff,100,stdin);
	//writing from input file to output file
	write(fp1,buff,strlen(buff));
	printf("writing to output file using system call successfull\n");
	return 0;

}

/*********************************************************************************
OUTPUT:
-------
[fedora@localhost Downloads]$ ./a.out 
input file opened in read mode
write the conent that should be written into file 
hi this tataji 
writing to output file using system call successfull
[fedora@localhost Downloads]$ cat Ttt.txt 
hi this tataji

**********************************************************************/

