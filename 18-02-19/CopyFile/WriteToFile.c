****************************************************************************************
 AUTHOR:
 -------
        TATAJI.IRRINKULA  Emp_ID-10347  email: tatajiirrinki@gmail.com  9704147867

        Purpose:
        -------
                This program is used to copy data from one file to another file using 
                system calls.


******************************************************************************************/
#include<stdio.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<unistd.h>

int main() {

	int fp1,fp2;
	int size=100;
	fp1=open("input.txt",O_RDONLY);
	fp2=open("output.txt",O_WRONLY | O_CREAT);
	if (fp1 == -1)

		printf("Error in opening the file  \n"); 
	else 
			printf("input file opened in read mode\n");
	if(fp2 == -1) 
		printf("Error in creating  the file  \n");
	else 
		printf("output file opened to write the conents of input file \n");
	//writing from input file to output file
	while(size) {
	char buff[size];
	size=read(fp1,buff,size);
	write(fp2,buff,size);
	}
	printf("writing to output file using system call successfull\n");
	return 0;
}

/****************************OUTPUT******************************

input file opened in read mode
output file opened to write the conents of input file 
writing to output file using system call successfull

******************************************************************/




