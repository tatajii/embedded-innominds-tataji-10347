#include<stdio.h>
#include<unistd.h>
#include<strings.h>
#include<string.h>
#include<stdlib.h>
#include<sys/types.h>
#include<netdb.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<netinet/ip.h>

int main(int argc,char* argv[]) {

	//checking whether the command line arguments are sufficient or not
	//if not printing the error message
	if(argc<5) {
		perror("insufficient command line arguments\n");
		exit(1);
	}

	//declaring the local variables
	int size=0,cfd;
	char buf[256];
	FILE *fp;
	char *buff=NULL;
	struct sockaddr_in ser_addr;
	struct hostent *server;
	
	//creating the client socket through socket system call 
	cfd=socket(AF_INET,SOCK_STREAM,0);
	//priting the error message if the socket system call fails
	if(cfd<0) {
		printf("ERROR:failed in opening the socket\n");
		exit(1);
	}
	
	//getting the server information through gethostbyname system call
	server=gethostbyname(argv[1]);

	//initializing the members of sockaddr_in structure
	ser_addr.sin_family=AF_INET;
	ser_addr.sin_port=htons(atoi(argv[2]));
	ser_addr.sin_addr.s_addr=INADDR_ANY;
	bcopy((char*)server->h_addr,(char*)&ser_addr.sin_addr.s_addr,server->h_length);

	//connecting to server socket through connect system call
	connect(cfd,(struct sockaddr *)&ser_addr,sizeof(ser_addr));

	//writing the file name whose content the client wants to get onto socket
	write(cfd,argv[3],strlen(argv[3]));
	sleep(2);
	//reading the size of the file through  read system call
	read(cfd,&size,sizeof(int));
	//reading the content of the file from socket to buffer
	buff=(char*)malloc(size*sizeof(char));
	read(cfd,buff,size);
	//opening the file to copy that content 
	fp=fopen(argv[4],"w");
	//writing the content of the buffer onto file opened previously
	fwrite(buff,size,1,fp);
	//closing the file descriptor which is opened by fopen system call
	fclose(fp);

	//returning from main
	return 0;

}
