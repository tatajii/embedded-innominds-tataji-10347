#include<stdio.h>
#include<unistd.h>
#include<strings.h>
#include<string.h>
#include<stdlib.h>
#include<sys/types.h>
#include<netdb.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<netinet/ip.h>

int main(int argc,char* argv[]) {

	if(argc<3) {
		perror("insufficient command line arguments\n");
		exit(1);
	}
	int sfd,cfd,portno;
	char buf[256];
	struct sockaddr_in ser_addr;
	struct hostent *server;
	
	cfd=socket(AF_INET,SOCK_STREAM,0);
	
	if(cfd<0) {
		printf("ERROR:failed in opening the socket\n");
		exit(1);
	}
	
	server=gethostbyname(argv[1]);

	portno=atoi(argv[2]);
	ser_addr.sin_family=AF_INET;
	ser_addr.sin_port=htons(portno);
	ser_addr.sin_addr.s_addr=INADDR_ANY;
	
	bcopy((char*)server->h_addr,(char*)&ser_addr.sin_addr.s_addr,server->h_length);

	connect(cfd,(struct sockaddr *)&ser_addr,sizeof(ser_addr));

while(1) {
	gets(buf);
	write(cfd,buf,strlen(buf));
	sleep(2);
	read(cfd,buf,255);
	printf("%s\n",buf);
}

	return 0;

}
