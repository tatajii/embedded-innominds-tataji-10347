#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<netinet/ip.h>

int main(int argc,char* argv[]) {

	if(argc<2) {
		perror("insufficient command line arguments\n");
		exit(1);
	}
	int sfd,cfd,portno;
	char buf[256]={'\0'};
	struct sockaddr_in ser_addr,cli_addr;
	
	sfd=socket(AF_INET,SOCK_STREAM,0);
	
	if(sfd<0) {
		printf("ERROR:failed in opening the socket\n");
		exit(1);
	}
	
	portno=atoi(argv[1]);
	ser_addr.sin_family=AF_INET;
	ser_addr.sin_port=htons(portno);
	ser_addr.sin_addr.s_addr=INADDR_ANY;
	
	if(bind(sfd,(struct sockaddr *)&ser_addr,sizeof(ser_addr))==-1) {
		perror("ERROR:binding failed\n");
		exit(1);
	}

	listen(sfd,1);
int len=sizeof(cli_addr);
	cfd=accept(sfd,(struct sockaddr *)&cli_addr,&len);

	if(cfd<0) {
		perror("ERROR:accept failed\n");
		exit(1);
	}
while(1) {

	read(cfd,buf,255);
	printf("%s\n",buf);
	gets(buf);
	write(cfd,buf,strlen(buf));
}

	return 0;

}
