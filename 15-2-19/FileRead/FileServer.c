#include<stdio.h>
#include<unistd.h>
#include<string.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<netinet/ip.h>

int main(int argc,char* argv[]) {

	//checking whether the command line arguments are sufficient or not
	//if not printing the error message
	if(argc<2) {
		perror("insufficient command line arguments\n");
		exit(1);
	}

	//declaring the local variables
	int sfd,cfd,portno;
	char buf[256]={'\0'};
	char *buff;
	FILE *fp;
	int size=0;
	socklen_t len=0;
	int flag=-1;
	struct sockaddr_in ser_addr,cli_addr;

	//creating the server socket through socket system call 
	sfd=socket(AF_INET,SOCK_STREAM,0);
	//priting the error message if the socket system call fails
	if(sfd<0) {
		printf("ERROR:failed in opening the socket\n");
		exit(1);
	}

	//initializing the members of sockaddr_in structure
	portno=htons(atoi(argv[1]));
	ser_addr.sin_family=AF_INET;
	ser_addr.sin_port=portno;
	ser_addr.sin_addr.s_addr=INADDR_ANY;

	//binding the server socket through bind system call
	//printing the error message if it fails
	if(bind(sfd,(struct sockaddr *)&ser_addr,sizeof(ser_addr))==-1) {
		perror("ERROR:binding failed\n");
		exit(1);
	}

	//listening to clients' request through listen system call
	//priting the error message if it fails
	if(listen(sfd,1)<0) {
		printf("ERROR:listen call failed\n");
		exit(1);
	}

	//accepting the client's request through accept system call
	len=sizeof(cli_addr);
	cfd=accept(sfd,(struct sockaddr *)&cli_addr,&len);
	//priting the error message if it fails
	if(cfd<0) {
		perror("ERROR:accept failed\n");
		exit(1);
	}
	
	//reading the file name sent by client 
	read(cfd,buf,255);
	//opening the given file if it is exist othrwise printing the error message
	fp=fopen(buf,"r");
	if(fp<0) {
		printf("can't open file:%s\n",buf);
		write(cfd,&flag,sizeof(int));
		exit(1);
	}

	//calculating the size of the given file
	fseek(fp,0,2);
	size=ftell(fp);
	rewind(fp);
	//writing the size onto client socket
	write(cfd,&size,sizeof(int));
	//reading the content of the given file
	buff=(char*)malloc(size*sizeof(char));
	fread(buff,size,1,fp);
	//writing it onto the client socket
	write(cfd,buff,strlen(buff));
	//closing the file descriptor which is opened by fopen system call
	fclose(fp);

	//returning from main
	return 0;

}
