/******************************************************************************
AUTHOUR:
--------
        TATAJI.IRRINKULA   emp Id:10347       emial: tatajiirrinki@gmail.com
    Purpose:
        In this algorithm , find the number of possible way to move from first element to last element
            in a matrix .
            note: move only farward direction --> and downward direction
*******************************************************************************/
#include<stdio.h>
#include<conio.h>

int main ()
{
  int i, j, row, column;
  clrscr ();
  printf ("enter no.of rows:");
  scanf ("%d", &row);
  while (row<=0) {
      printf("enter the positive integer value:");
      scanf("%d", &row);
  }
  printf ("enter no.of columns: ");
  scanf ("%d", &column);
    while (column<=0) {
      printf("enter the positive integer value:");
      scanf("%d", &column);
  }
  
  int a[row][column];

  for (i = 0; i < row; i++)
    {
      for (j = 0; j < column; j++)
	{
	  if (i == 0 || j == 0)
	    {
	      a[i][j] = 1;
	    }
	}
    }
  for (i = 0; i < row - 1; i++)
    {
      for (j = 1; j < column; j++)
	{
	  a[i + 1][j] = a[i + 1][j - 1] + a[i][j];
	}
    }
  printf ("no_of_ways move first element to last element in a %d x %d maatrix is :%d", row,column,a[row - 1][column - 1]);

  getch ();
  return 0;
}
/******************************************************************************
OUTPUT :
-------
    enter no.of rows:-5
    enter the positive integer value:4
    enter no.of columns:-9
    enter the positive integer value:7
    no_of_ways move first element to last element in a 4 x 7 maatrix is :84
    
    
    

*******************************************************************************/