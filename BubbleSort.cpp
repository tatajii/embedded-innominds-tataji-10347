/****************************************************************************************************** 

  Author          
  -------
  I.TATAJI        10347   tatajiirrinki@gmail.com     Cell : 9704147867

Purpose:
--------
	To sort the given array elents by using Bubble Sort technique.



*********************************************************************************************************/

#include <iostream>
#include<stdlib.h>
using namespace std;
// creating class BubbleSort
class BubbleSort {
	// declaring data members
	int *myarray;
	int size;
	public:
	// parameterised constructor for BubbleSort class
	BubbleSort(int size) {
		this->size=size;
		myarray= new int[size];
		cout<<"enter array elements:"<<endl;
		for(int i=0;i<size;i++)
			cin>>myarray[i];
	}
	// method for sorting the elements in an array
	void bubbleSort() {
		for(int i=0;i<size;i++)
			for(int j=0;j<size-1;j++)
				if(myarray[j]>myarray[j+1])
					swap(myarray[j],myarray[j+1]);
	}
	// method for printing the array elements
	void printArray() {
		for(int i=0;i<size;i++)
			cout<<myarray[i]<<" ";
		cout<<endl;
	}
	// method for swapping two  elements
	void swap(int &a,int &b) {
		int temp;
		temp=a;
		a=b;
		b=temp;
	}
};

int main()
{
	// creating object for BubbleSort class and passing the array size
	BubbleSort object(5);
	cout<<"array elements before sorting:"<<endl;
	// calling print array method 
	object.printArray();
	// calling bubblesort method 
	object.bubbleSort();
	cout<<"array elements after sorting:"<<endl;
	// calling print array method
	object.printArray();
	return 0;
}
/********************************************************************************************************
OUTPUT:
-------
enter array elements:
8
6
9
3
0
array elements before sorting:
8 6 9 3 0 
array elements after sorting:
0 3 6 8 9 

**********************************************************************************************************/
