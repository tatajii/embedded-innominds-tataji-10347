/******************************************************************************
AUTHOUR:
--------
        TATAJI.IRRINKULA   emp Id:10347       emial: tatajiirrinki@gmail.com
    purpose:
    --------
            simple programs using pointers

*******************************************************************************/
#include <stdio.h>

int main()
{
    int a=5;
    int *ptr1=NULL;
    int **ptr2=NULL;
    
    // address of a is assign to ptr1 
      ptr1=&a;
     //ptr1 gives the address of a
    printf("\n%d\n",ptr1);
    ptr1=a;
    // here a is assign to ptr1 , we cannot assign integer to pointer variable
    printf("%d\n",ptr1);
    ptr2=&ptr1;
    //ptr2 gives the address of ptr1
    printf("%d\n",ptr2);
    // 10 is assign to **ptr2 , because of this ptr1=a; it gives Segmentationfault
    **ptr2=10;
    printf("%d\n",a);
    
    return 0;
}
/**************************************************************************************
 OUTPUT:
 --------
    -1230110804
    5
    -1230110800
    Segmentation fault (core dumped)
****************************************************************************************/
