/******************************************************************************
AUTHOUR:
--------
        TATAJI.IRRINKULA   emp Id:10347       emial: tatajiirrinki@gmail.com
    purpose:
    --------
            simple programs using pointers

*******************************************************************************/
#include <stdio.h>

int main()
{
    int a=5,b;
    // here asigning address of a to *p1
    int *p1=&a;
    //if we print the *p1, it gives the same value of a 
    printf("%d\n",*p1);
    //here assigning 7 to *p1
    *p1=7;
    // *p1 changes the value of a=5 to a=7
    printf("%d\n",a);
    // pre increment the value of *p1
    ++*p1;
    // *p1 changes the value of a=7 to a=8
    printf("%d\n",a);
    b=a;
    //printing value b
    printf("%d\n",b);
    
    b=*p1;
    //*p1 value assign to b
    printf("%d\n",b);

    return 0;
}
/**************************************************************************************
 OUTPUT:
 --------
        5
        7
        8
        8
        8
****************************************************************************************/
