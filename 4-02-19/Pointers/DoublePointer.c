/******************************************************************************
AUTHOUR:
--------
        TATAJI.IRRINKULA   emp Id:10347       emial: tatajiirrinki@gmail.com
    purpose:
    --------
            simple programs using pointers

*******************************************************************************/
#include <stdio.h>

int
main ()
{
  int a = 5, b = 10,*ptr1,**ptr2,c=0;
  ptr1 = &c;
  ptr2=&ptr1;

  printf ("%d\n", &ptr2);
  

  add (a, b, ptr2);
  printf ("%d\n", c);

  return 0;
}

void add (int a, int b, int **ptr)
{
  
  printf ("%d\n", &ptr);
 **ptr = a + b;
  
}



/**************************************************************************************
 OUTPUT:
 --------
    1672686520
    1672686464
       15
****************************************************************************************/