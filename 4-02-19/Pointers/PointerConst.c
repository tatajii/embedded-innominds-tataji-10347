/******************************************************************************
AUTHOUR:
--------
        TATAJI.IRRINKULA   emp Id:10347       emial: tatajiirrinki@gmail.com
    purpose:
    --------
            simple programs using pointers

*******************************************************************************/
#include <stdio.h>

int
main ()
{
  int c=10;
  const int *ptr= &c;
  func(ptr);
  printf("%d\n",c);

  return 0;
}
void func(int *ptr)
//in place of int*ptr,we give const int *ptr then it shows error 
{
  int a=0;
  a=*ptr;
  *ptr=80;
}



/**************************************************************************************
 OUTPUT:
 --------
    80
****************************************************************************************/