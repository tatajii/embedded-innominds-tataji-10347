/******************************************************************************
AUTHOUR:
--------
        TATAJI.IRRINKULA   emp Id:10347       emial: tatajiirrinki@gmail.com
    purpose:
    --------
            simple programs using pointers

*******************************************************************************/
#include <stdio.h>

int main()
{
    int array[5]={1,2,3,4,5};
    int *ptr1=NULL;
    int **ptr2=NULL;
    for (int i=0;i<5;i++) {
        printf("%d  ",array[i]);
    }
    // address of array is assign to ptr1 
      ptr1=&array;
     //ptr1 gives the address of array
    printf("\n%d\n",ptr1);
    ptr1=array;
    // here also ptr1 gives address of array
    printf("%d\n",ptr1);
    ptr2=&ptr1;
    //ptr1 gives the address of ptr1
    printf("%d\n",ptr2);
    // 10 is assign to **ptr2 it changes the first element in the array
    **ptr2=10;
    
    printf("%d\n",array[0]);
    for (int i=0;i<5;i++) {
        printf("%d  ",array[i]);
    }
    return 0;
}
/**************************************************************************************
 OUTPUT:
 --------
    1  2  3  4  5
    -1473629744
    -1473629744
    -1473629760
    10
    10  2  3  4  5
****************************************************************************************/
