/******************************************************************************
AUTHOUR:
--------
        TATAJI.IRRINKULA   emp Id:10347       emial: tatajiirrinki@gmail.com
    purpose:
    --------
            simple programs using pointers

*******************************************************************************/
#include <stdio.h>

int main()
{
    int *p1;
    
    *p1=6;
    /* it gives error because. 
       *p1=null;
    *(null)=5 ; *null has no address and doesnt contain any value */
    printf("%d\n",*p1);

    return 0;
}
/**************************************************************************************
 OUTPUT:
 --------
        Segmentation fault(core dumped)
****************************************************************************************/
