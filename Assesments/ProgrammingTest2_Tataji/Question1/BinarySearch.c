 /**********************************************************************************************************************************************
AUTHOR:
------
	TATAJI IRRINKULA	Emp_Id-10347	email: tatajiirrinki@gmail.com		cell: 9704147867
	
	Purpose:
	--------
		In this program, seraching a element in a sorted array by using binary search recursion.

**************************************************************************************************************************************************/
// header section

#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>

// binarySearch function to serch the element in array ,by using recurrsion.
int binarySearch(int low,int high,int *array,int data) {
	
	int mid;
	if(low > high) {
		return -1;
	}
	mid=(low + high)/2;

	if (array[mid] == data) {
		return mid;
	}
	else if(array[mid] > data) {
		high = mid - 1;
		return(binarySearch(low,high,array,data));
	}
	else {
		low = mid +1;
		return(binarySearch(low,high,array,data));
	}
}

// main funtion starts here
int main() {
	
	// initialising variables
	int array[11] = {3,5,7,9,10,14,16,35,67,89,90};
	int data,index;
	// taking search element and stored in data variable
	printf(" enter the data you want to be search : \n");
	scanf("%d",&data);
	
	//  here collecting the  search element in index
	index = binarySearch(0,10,array,data);
	
	// checking search element is present or not
	if (index >= 0) {
		printf(" the serch element %d is present in position=%d \n ",data,index+1);
		
	}
	else 
		printf(" search element is not present \n");
	
	return 0;
}








