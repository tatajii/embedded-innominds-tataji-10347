// here include all header section by using headers.h
#include<headers.h>

// global variables initialising 
int sockfd,portnum,n;
char* buff;
char buf[] ={'b','y','e','\n'};

//declaring errorfunction for error message
 void * error (char *msg) {
	 perror(msg);
	 exit(1);
 }


// clientRead function to read the data from client
void * clientRead(char *arg) {
	while(1) {
		__fpurge(buff);
		n=read(sockfd,buff,strlen(buff));
		if(n<0) {
			error("error in reading\n");
		}
		if(!(strcmp(buff,buf))) {
			printf("server=%s \n",buff);
			break;
		}
		else 
			printf("server=%s \n",buff);
	}
	return NULL;
	
}
//clientWrite function to write the message to client
void * clientWrite(char *arg) {
	while(1) {
		__fpurge(buff);
		fgets(buff,1024,stdin);
		n=write(sockfd,buff,strlen(buff));
		if(n<0) {
			error("error in reading\n");
		}

		if(!(strcmp(buff,buf))) {
			printf("client=%s \n",buff);
			break;
		}
	
}
return NULL;
}
// main function starts here, it contains argc and argv parameters.

int main(int argc , char **argv) {

	// initialising variables
	struct sockaddr_in serv_addr;
	struct hostent *server;
	pthread_t thread_id,thread_id1;

	// checking arguements entered in client commandprompt
	if(argc<2 || argc >2) {
		printf("insufficient arguements porting is not possible");
	}
	// assign socket to sockfd
	sockfd=socket(AF_INET,SOCK_STREAM,0);
	//checking socket is connect or not
	if(sockfd < 0) {
		error("error in coonect socket");
	}
	 // taking portnumber from command prompt
	portnum = atoi(argv[1]);
	
	// checking server
	server=gethostbyname(argv[1]);
	if(server==NULL)
		error("error in server");
	memset((char*)&serv_addr,0,sizeof(serv_addr));
	
	
	serv_addr.sin_family=AF_INET;
	strncpy((char*) &server->h_length,(char*)&serv_addr.sin_addr.s_addr,server->h_length);
	serv_addr.sin_port=htons(portnum);
	
	// checking the binding process
	if(connect(sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr))<0) {
	       error("error in connecting");
	}
	// creating threads
	pthread_create(&thread_id,NULL,clientRead,NULL);
	pthread_create(&thread_id1,NULL,clientWrite,NULL);
	
	// joining threads
	pthread_join(thread_id,NULL);
	pthread_join(thread_id1,NULL);
	
	close(sockfd);
	
	return 0;
}




