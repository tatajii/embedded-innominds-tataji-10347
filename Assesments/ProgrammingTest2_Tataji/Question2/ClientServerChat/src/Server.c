// here include all header section by using headers.h
#include<headers.h>

// global variables initialising 
int sockfd,newsockfd,portnum,n;
char* buff;
char buf[] ={'b','y','e','\n'};

//declaring errorfunction for error message
 void * error (char *msg) {
	 perror(msg);
	 exit(1);
 }


// serverRead function to read the data from client
void * serverRead(char *arg) {
	while(1) {
		__fpurge(buff);
		n=read(newsockfd,buff,strlen(buff));
		if(n<0) {
			error("error in reading\n");
		}
		if(!(strcmp(buff,buf))) {
			printf("client=%s \n",buff);
			break;
		}
		else 
			printf("client=%s \n",buff);
	}
	return NULL;
	
}
//serverWrite function to write the message to client
void * serverWrite(char *arg) {
	while(1) {
		__fpurge(buff);
		fgets(buff,1024,stdin);	
		n=write(newsockfd,buff,strlen(buff));
		if(n<0) {
			error("error in reading\n");
		}

		if(!(strcmp(buff,buf))) {
			printf("client=%s \n",buff);
			break;
		}
	

}
return NULL;
}
// main function starts here, it contains argc and argv parameters.

int main(int argc , char **argv) {

	// initialising variables
	struct sockaddr_in serv_addr,cli_addr;
	socklen_t cli_len;
	pthread_t thread_id,thread_id1;

	// checking arguements entered in client commandprompt
	if(argc<3 || argc >3) {
		printf("insufficient arguements porting is not possible");
	}
	// assign socket to sockfd
	sockfd=socket(AF_INET,SOCK_STREAM,0);
	//checking socket is connect or not
	if(sockfd < 0) {
		error("error in coonect socket");
	}
	 // taking portnumber from command prompt
	portnum = atoi(argv[1]);

	memset((char*)&serv_addr,0,sizeof(serv_addr));
	
	
	serv_addr.sin_family=AF_INET;
	serv_addr.sin_addr.s_addr=INADDR_ANY;
	serv_addr.sin_port=htons(portnum);
	
	// checking the binding process
	if(bind(sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr))<0) {
	       error("error in connecting");
	}
	// listen
	listen(sockfd,5);
	// getting clientlength
	cli_len=sizeof(cli_addr);
	newsockfd=accept(sockfd,(struct sockaddr *)&cli_addr,&cli_len) ;
	if(newsockfd < 0) {
		error("error in accepting");


	}
	// creating threads
	pthread_create(&thread_id,NULL,serverRead,NULL);
	pthread_create(&thread_id1,NULL,serverWrite,NULL);
	
	// joining threads
	pthread_join(thread_id,NULL);
	pthread_join(thread_id1,NULL);
	
	close(sockfd);
	close(newsockfd);
	return 0;
}




