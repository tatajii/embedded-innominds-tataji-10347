INSTRUCTIONS: 
=============

Read instructions and questions carefully as there are marks for understanding the questions.
If any doubts in the question, clarify it before starting to wrut ethe code.

Time: 3hrs 3o mins
Max Marks: 30

The question paper contains four questions, each carrying 10 marks. 
You need to attempt any three of them.
Create directory for each question.
if any extra files needed to be created to demonstrate the working of code, create them inside "etc" directory which is placed parallel to the bin directory.


5 to 35 marks will be deducted for cheating, copying, talking to others and using internet during the exam.
switch off your mobile phone and submit on my desk.
You must be connected to the D-link wifi throughout the exam so that I can monitor your activities. 

Distribution of marks (out of 10) per question:
	Successful demonstration of result		: 2 marks
	Successful compilation				: 2 marks
	Appropriate Logic				: 2 marks
	understanding of question			: 2 marks
	Cleanliness and readability of the code 	: 2 marks
***********************************************************************************

Q1) Create an sorted array with elements: 3,5,7,9,10,14,16,35,67,89,90
Don't take input from user to create the array. Ask user for an integer to search, if it exists, print its position else print "it does not exist."
use recurssive model of binary search. No makefile necessary.

Ex: please enter a number to search:
7
position of 7 is 3

please enter a number to search:
8
it does not exist.

*****************************

Q2) client-server continuous chat using socket communication and threads.
if client or server sends "bye" then the chatting is over and processes on both server and client side gets terminated. 
Use one makefile to create both server and client executables.

********************************
Q3) Show the content of a file inside a directory.
Explanation:
Ask user to provide a directory name. 
Ask user to provide which file (latest or largest) content he/she wants to see. 
Then display the content of the file on stdout using cat command through exec() and fork().
Use only system calls to implement the problem. No makefile necessary.

Ex:
Provide the directory path
/home/user/Desktop/
You want to see latest file or largest file?
latest
Content of latest file is:
....
....
....
....
You want to see latest file or largest file?
largest
Content of largest file is:
....
....
....

**********************************
Q4) Build a client server program to download a file from server.

Explanation.
In client side ask user for a file name.
send the file name to server. If the file is present in server side send the content to client and client will save the file.
If the file does not exist server sends "No such files..."
use one makefile to generate both the executables.













