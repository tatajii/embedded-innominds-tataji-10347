/*********************************************************************************************************************************************
AUTHOR:
-------
	TATAJI IRRINKULA	tatajiirrinki@gmail.com	    Emp-Id:10347 	Ph.No:9704147867
	Purpose:
	-------
		This program is used to find the missing number in a sorted order by using binary search tree. 
*****************************************************************************************************************************************/


#include<stdio.h>
#include<stdlib.h>

//Declarations for functions that are used in program
int *sort(int *,int );
int binarysearch(int ,int,int *,int); 

int main() {
	//declaring the instant variables and array
	int array[100];
	int size,i,data,index;

	printf("enter the size\n");
	scanf("%d",&size);
	printf("enter the numbers\n");

	//inputing the numbers into the array
	for(i=0;i<size;i++) {
		scanf("%d",&array[i]);
	}

	//sort the given array
	sort(array,size);

	//printing the sorted array
	for(i=0;i<size;i++) {
		printf("%d\t",array[i]);
	}
	printf("\n");

	printf("enter the data to be searched\n");
	scanf("%d",&data);

	//calling the binarysearch function
	index=binarysearch(0,size-1,array,data);

	if(index>=0)
		printf("the data is found at %d index\n",index);
	else
		printf("the data is not found\n");


}

//This function is used to sort the unsorted array and return to the main

int * sort(int *array,int size) {

	int i,j,temp;

	//logic for sorting //bubble sort
	for(i=0;i<size;i++) {
		for(j=0;j<size-1;j++) {
			if(array[j] > array[j+1]) {
				temp = array[j];
				array[j]=array[j+1];
				array[j+1]=temp;
			}
		}
	}

	return array;
}

//This is the logic to search the given element by using binarysearch tree
int binarysearch(int low,int high,int *array,int data) {

	int mid;

	if(low>high)
		return -1;
	mid=(low+high)/2;

	if(array[mid]==data)
		return mid;

	else if(array[mid]>data) {
		high = mid-1;
		binarysearch(low,high,array,data);
	}

	else {
		low=mid+1;
		binarysearch(low,high,array,data);
	}
}

/******************************************************************************************************************************************
OUTPUT:
-------
enter the size
5
enter the numbers
0
-4
5
2
89
-4	0	2	5	89	
enter the data to be searched
2
the data is found at 2 index

******************************************************************************************************************************************/
