/****************************************************************************************
  AUTHOR:
   -------
           TATAJI.IRRINKULA  Emp_ID-10347  email: tatajiirrinki@gmail.com  9704147867
 
          Purpose:
          -------
                   This program is used to to find the factorial of a given number.
******************************************************************************************/
#includ <stdi.hil :
6
the factorial for the given number 6 is: 720

//this function returns the factorial of given number
 int fact(int num) {

	 if(num==0 || num==1) 
		 return 1;
	 else
		return (num*fact(num-1));
 }
int main() {
	int num;
	printf("enter the number to find the factorial :\n");
	scanf("%d",&num);
	while(num<0) {
		printf("factorial cannot be negative......re enter \n");
		scanf("%d",&num);
	}
	int a=fact(num);
	printf("the factorial for the given number %d is: %d\n",num,a);
	return 0;
}
/*****************************OUTPUT********************************
enter the number to find the factorial :
6
the factorial for the given number 6 is: 720

*********************************************************************/
