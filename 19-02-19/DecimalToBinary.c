/***************************************************************************************
  AUTHOR:
 -------
        TATAJI.IRRINKULA  Emp_ID-10347  email: tatajiirrinki@gmail.com  9704147867

        Purpose:
        -------
                This program is used to convert decimal to binary number.

 * ***************************************************************************************/
#includ
<stdio.h>
#includ
<stdlib.h>
//it converts given decimal number in  binary number and returns the value  
int decimalToBinary(int num) {
	if(num==0) 
		return 0;
	else 
		return (num%2+10 * (decimalToBinary(num/2)));

}

int main() {
	int num;
	printf("enter the number\n");
	scanf("%d",&num);
	int a=decimalToBinary(num);
	printf("the binary  number is %d  for the given decimal number  %d\n",a,num);
	return 0;
}
/**********************************************************************************
 OUTPUT:
-------
enter the number
23
the  binary  number is 10111  for the given decimal number  23
****************************************************************************************/
